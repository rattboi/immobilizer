EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 3750 3900 550  2550
U 5E89CD30
F0 "AV Out" 50
F1 "Video_Out.sch" 50
F2 "VID_R0" I L 3750 4250 50 
F3 "VID_R1" I L 3750 4350 50 
F4 "VID_R2" I L 3750 4450 50 
F5 "VID_R3" I L 3750 4550 50 
F6 "VID_R4" I L 3750 4650 50 
F7 "VID_R5" I L 3750 4750 50 
F8 "VID_G0" I L 3750 4900 50 
F9 "VID_G1" I L 3750 5000 50 
F10 "VID_G2" I L 3750 5100 50 
F11 "VID_G3" I L 3750 5200 50 
F12 "VID_G4" I L 3750 5300 50 
F13 "VID_G5" I L 3750 5400 50 
F14 "VID_B0" I L 3750 5550 50 
F15 "VID_B1" I L 3750 5650 50 
F16 "VID_B2" I L 3750 5750 50 
F17 "VID_B3" I L 3750 5850 50 
F18 "VID_B4" I L 3750 5950 50 
F19 "VID_B5" I L 3750 6050 50 
F20 "VID_HS" I L 3750 6250 50 
F21 "VID_VS" I L 3750 6350 50 
F22 "SND_L" I L 3750 4000 50 
F23 "SND_R" I L 3750 4100 50 
$EndSheet
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J4
U 1 1 5E8432A3
P 6850 4900
F 0 "J4" H 6900 6017 50  0000 C CNN
F 1 "Conn_02x20_Odd_Even" H 6900 5926 50  0000 C CNN
F 2 "immobilizer_lib:PinHeader_2x20_P2.54mm_Vertical" H 6850 4900 50  0001 C CNN
F 3 "~" H 6850 4900 50  0001 C CNN
	1    6850 4900
	1    0    0    -1  
$EndComp
Text GLabel 7300 4000 2    50   Input ~ 0
VCCIO3
Text GLabel 6450 4000 0    50   Input ~ 0
VCC33
Text GLabel 6400 4300 0    50   Input ~ 0
GND
Text GLabel 7300 4300 2    50   Input ~ 0
GND
Text GLabel 6400 4700 0    50   Input ~ 0
GND
Text GLabel 7300 4700 2    50   Input ~ 0
GND
Text GLabel 6400 5100 0    50   Input ~ 0
GND
Text GLabel 7300 5100 2    50   Input ~ 0
GND
Text GLabel 7300 5500 2    50   Input ~ 0
GND
Text GLabel 6400 5500 0    50   Input ~ 0
GND
Text GLabel 7300 5900 2    50   Input ~ 0
GND
Text GLabel 6400 5900 0    50   Input ~ 0
GND
Wire Wire Line
	6650 4000 6550 4000
Wire Wire Line
	6650 4100 6550 4100
Wire Wire Line
	6550 4100 6550 4000
Connection ~ 6550 4000
Text GLabel 9750 1050 2    50   Input ~ 0
VCCIO2
Text GLabel 8850 1350 0    50   Input ~ 0
GND
Text GLabel 9750 1350 2    50   Input ~ 0
GND
Text GLabel 8850 1750 0    50   Input ~ 0
GND
Text GLabel 9750 1750 2    50   Input ~ 0
GND
Text GLabel 8850 2150 0    50   Input ~ 0
GND
Text GLabel 9750 2150 2    50   Input ~ 0
GND
Text GLabel 8850 2550 0    50   Input ~ 0
GND
Text GLabel 9750 2550 2    50   Input ~ 0
GND
Text GLabel 8850 2950 0    50   Input ~ 0
GND
Text GLabel 9750 2950 2    50   Input ~ 0
GND
Text GLabel 8850 1050 0    50   Input ~ 0
R16
Text GLabel 8850 1150 0    50   Input ~ 0
T15
Text GLabel 8850 1250 0    50   Input ~ 0
T13
Text GLabel 8850 1450 0    50   Input ~ 0
N12
Text GLabel 8850 1550 0    50   Input ~ 0
N10
Text GLabel 8850 1650 0    50   Input ~ 0
T11
Text GLabel 8850 1850 0    50   Input ~ 0
T10
Text GLabel 8850 1950 0    50   Input ~ 0
P8
Text GLabel 8850 2050 0    50   Input ~ 0
T9
Text GLabel 8850 2250 0    50   Input ~ 0
T7
Text GLabel 8850 2350 0    50   Input ~ 0
T6
Text GLabel 8850 2450 0    50   Input ~ 0
T5
Text GLabel 8850 2650 0    50   Input ~ 0
R3
Text GLabel 8850 2750 0    50   Input ~ 0
R2
Text GLabel 8850 2850 0    50   Input ~ 0
T1
Text GLabel 9750 1150 2    50   Input ~ 0
T16
Text GLabel 9750 1250 2    50   Input ~ 0
T14
Text GLabel 9750 1450 2    50   Input ~ 0
P13
Text GLabel 9750 1550 2    50   Input ~ 0
M11
Text GLabel 9750 1650 2    50   Input ~ 0
P10
Text GLabel 9750 1850 2    50   Input ~ 0
R10
Text GLabel 9750 1950 2    50   Input ~ 0
P9
Text GLabel 9750 2050 2    50   Input ~ 0
R9
Text GLabel 9750 2250 2    50   Input ~ 0
T8
Text GLabel 9750 2350 2    50   Input ~ 0
R6
Text GLabel 9750 2450 2    50   Input ~ 0
R5
Text GLabel 9750 2650 2    50   Input ~ 0
R4
Text GLabel 9750 2750 2    50   Input ~ 0
T3
Text GLabel 9750 2850 2    50   Input ~ 0
T2
Text GLabel 6400 4200 0    50   Input ~ 0
P1
Text GLabel 6400 4400 0    50   Input ~ 0
N3
Text GLabel 6400 4500 0    50   Input ~ 0
M2
Text GLabel 6400 4600 0    50   Input ~ 0
L3
Text GLabel 6400 4800 0    50   Input ~ 0
K3
Text GLabel 6400 4900 0    50   Input ~ 0
J2
Text GLabel 6400 5000 0    50   Input ~ 0
H2
Text GLabel 6400 5200 0    50   Input ~ 0
G2
Text GLabel 6400 5300 0    50   Input ~ 0
F2
Text GLabel 6400 5400 0    50   Input ~ 0
E2
Text GLabel 6400 5600 0    50   Input ~ 0
D1
Text GLabel 6400 5700 0    50   Input ~ 0
C1
Text GLabel 6400 5800 0    50   Input ~ 0
B1
Text GLabel 7300 4100 2    50   Input ~ 0
R1
Text GLabel 7300 4200 2    50   Input ~ 0
P2
Text GLabel 7300 4400 2    50   Input ~ 0
N2
Text GLabel 7300 4500 2    50   Input ~ 0
M1
Text GLabel 7300 4600 2    50   Input ~ 0
L1
Text GLabel 7300 4800 2    50   Input ~ 0
K1
Text GLabel 7300 4900 2    50   Input ~ 0
J1
Text GLabel 7300 5000 2    50   Input ~ 0
J3_CLK12MHz
Text GLabel 7300 5200 2    50   Input ~ 0
H1
Text GLabel 7300 5300 2    50   Input ~ 0
G1
Text GLabel 7300 5400 2    50   Input ~ 0
F1
Text GLabel 7300 5600 2    50   Input ~ 0
D2
Text GLabel 7300 5700 2    50   Input ~ 0
C2
Text GLabel 7300 5800 2    50   Input ~ 0
B2
Text GLabel 7500 1050 2    50   Input ~ 0
AD0
Text GLabel 7500 1150 2    50   Input ~ 0
AD1
Text GLabel 7500 1250 2    50   Input ~ 0
AD2
Text GLabel 7500 1350 2    50   Input ~ 0
AD3
Text GLabel 7500 1450 2    50   Input ~ 0
AD4
Text GLabel 7500 1650 2    50   Input ~ 0
IO0
Text GLabel 7500 1750 2    50   Input ~ 0
IO1
Text GLabel 7500 2050 2    50   Input ~ 0
VCC33
Text GLabel 7500 2150 2    50   Input ~ 0
GND
Text GLabel 7500 1850 2    50   Input ~ 0
IO2
Text GLabel 7500 1950 2    50   Input ~ 0
IO3
Text GLabel 7500 2650 2    50   Input ~ 0
~WE
Text GLabel 7500 2750 2    50   Input ~ 0
AD5
Text GLabel 7500 2850 2    50   Input ~ 0
AD6
Text GLabel 7500 2950 2    50   Input ~ 0
AD7
Text GLabel 7500 3050 2    50   Input ~ 0
AD8
Text GLabel 7500 3150 2    50   Input ~ 0
AD9
Text GLabel 8050 1050 0    50   Input ~ 0
AD17
Text GLabel 8050 1150 0    50   Input ~ 0
AD16
Text GLabel 8050 1250 0    50   Input ~ 0
AD15
Text GLabel 7500 2550 2    50   Input ~ 0
IO7
Text GLabel 7500 2450 2    50   Input ~ 0
IO6
Text GLabel 8050 2050 0    50   Input ~ 0
GND
Text GLabel 8050 2150 0    50   Input ~ 0
VCC33
Text GLabel 7500 2350 2    50   Input ~ 0
IO5
Text GLabel 7500 2250 2    50   Input ~ 0
IO4
Text GLabel 8050 2750 0    50   Input ~ 0
AD14
Text GLabel 8050 2850 0    50   Input ~ 0
AD13
Text GLabel 8050 2950 0    50   Input ~ 0
AD12
Text GLabel 8050 3050 0    50   Input ~ 0
AD11
Text GLabel 8050 3150 0    50   Input ~ 0
AD10
Text GLabel 9500 6400 0    50   Input ~ 0
GND
Text GLabel 9500 6100 0    50   Input ~ 0
VCC33
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J3
U 1 1 5E843375
P 9300 1950
F 0 "J3" H 9350 3067 50  0000 C CNN
F 1 "Conn_02x20_Odd_Even" H 9350 2976 50  0000 C CNN
F 2 "immobilizer_lib:PinHeader_2x20_P2.54mm_Vertical" H 9300 1950 50  0001 C CNN
F 3 "~" H 9300 1950 50  0001 C CNN
	1    9300 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5E84337B
P 10100 6250
F 0 "C2" H 10192 6296 50  0000 L CNN
F 1 "100nF" H 10192 6205 50  0000 L CNN
F 2 "KiCad/kicad-footprints/Capacitor_SMD.pretty:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 10100 6250 50  0001 C CNN
F 3 "~" H 10100 6250 50  0001 C CNN
	1    10100 6250
	1    0    0    -1  
$EndComp
Wire Wire Line
	9650 6100 10100 6100
Text GLabel 8050 2550 0    50   Input ~ 0
IO8
Text GLabel 8050 2450 0    50   Input ~ 0
IO9
Text GLabel 8050 2350 0    50   Input ~ 0
IO10
Text GLabel 8050 2250 0    50   Input ~ 0
IO11
Text GLabel 8050 1950 0    50   Input ~ 0
IO12
Text GLabel 8050 1850 0    50   Input ~ 0
IO13
Text GLabel 8050 1750 0    50   Input ~ 0
IO14
Text GLabel 8050 1650 0    50   Input ~ 0
IO15
Text GLabel 8050 1350 0    50   Input ~ 0
~OE
$Comp
L immobilizer_sym:fpga_shield2_IS61LV25616 U1
U 1 1 5E843390
P 5850 1900
F 0 "U1" H 5850 1150 50  0000 C CNN
F 1 "fpga_shield2_IS61LV25616" V 5850 2000 50  0000 C CNN
F 2 "KiCad/kicad-footprints/Package_SO.pretty:TSOP-II-44_10.16x18.41mm_P0.8mm" H 5350 3050 50  0001 C CNN
F 3 "" H 5850 1900 50  0001 C CNN
	1    5850 1900
	1    0    0    -1  
$EndComp
Text GLabel 5150 800  0    50   Input ~ 0
AD0
Wire Wire Line
	5150 800  5250 800 
Text GLabel 5150 900  0    50   Input ~ 0
AD1
Wire Wire Line
	5150 900  5250 900 
Text GLabel 5150 1000 0    50   Input ~ 0
AD2
Wire Wire Line
	5150 1000 5250 1000
Text GLabel 5150 1100 0    50   Input ~ 0
AD3
Wire Wire Line
	5150 1100 5250 1100
Text GLabel 5150 1200 0    50   Input ~ 0
AD4
Wire Wire Line
	5150 1200 5250 1200
Text GLabel 5150 1300 0    50   Input ~ 0
AD5
Wire Wire Line
	5150 1300 5250 1300
Text GLabel 5150 1400 0    50   Input ~ 0
AD6
Wire Wire Line
	5150 1400 5250 1400
Text GLabel 5150 1500 0    50   Input ~ 0
AD7
Wire Wire Line
	5150 1500 5250 1500
Text GLabel 5150 1600 0    50   Input ~ 0
AD8
Wire Wire Line
	5150 1600 5250 1600
Text GLabel 5150 1700 0    50   Input ~ 0
AD9
Wire Wire Line
	5150 1700 5250 1700
Text GLabel 5150 1800 0    50   Input ~ 0
AD10
Wire Wire Line
	5150 1800 5250 1800
Text GLabel 5150 1900 0    50   Input ~ 0
AD11
Wire Wire Line
	5150 1900 5250 1900
Text GLabel 5150 2000 0    50   Input ~ 0
AD12
Wire Wire Line
	5150 2000 5250 2000
Text GLabel 5150 2100 0    50   Input ~ 0
AD13
Wire Wire Line
	5150 2100 5250 2100
Text GLabel 5150 2200 0    50   Input ~ 0
AD14
Wire Wire Line
	5150 2200 5250 2200
Text GLabel 5150 2300 0    50   Input ~ 0
AD15
Wire Wire Line
	5150 2300 5250 2300
Text GLabel 5150 2400 0    50   Input ~ 0
AD16
Text GLabel 5150 2500 0    50   Input ~ 0
AD17
Wire Wire Line
	5150 2400 5250 2400
Wire Wire Line
	5250 2500 5150 2500
Text GLabel 5150 2750 0    50   Input ~ 0
~OE
Wire Wire Line
	5150 2750 5250 2750
Text GLabel 5150 2850 0    50   Input ~ 0
~WE
Wire Wire Line
	5150 2850 5250 2850
Text GLabel 6550 800  2    50   Input ~ 0
IO0
Text GLabel 6550 900  2    50   Input ~ 0
IO1
Text GLabel 6550 1000 2    50   Input ~ 0
IO2
Text GLabel 6550 1100 2    50   Input ~ 0
IO3
Text GLabel 6550 1200 2    50   Input ~ 0
IO4
Text GLabel 6550 1300 2    50   Input ~ 0
IO5
Text GLabel 6550 1400 2    50   Input ~ 0
IO6
Text GLabel 6550 1500 2    50   Input ~ 0
IO7
Text GLabel 6550 1600 2    50   Input ~ 0
IO8
Text GLabel 6550 1700 2    50   Input ~ 0
IO9
Text GLabel 6550 1800 2    50   Input ~ 0
IO10
Text GLabel 6550 1900 2    50   Input ~ 0
IO11
Text GLabel 6550 2000 2    50   Input ~ 0
IO12
Text GLabel 6550 2100 2    50   Input ~ 0
IO13
Text GLabel 6550 2200 2    50   Input ~ 0
IO14
Text GLabel 6550 2300 2    50   Input ~ 0
IO15
Wire Wire Line
	6450 800  6550 800 
Wire Wire Line
	6550 900  6450 900 
Wire Wire Line
	6450 1000 6550 1000
Wire Wire Line
	6550 1100 6450 1100
Wire Wire Line
	6450 1200 6550 1200
Wire Wire Line
	6550 1300 6450 1300
Wire Wire Line
	6450 1400 6550 1400
Wire Wire Line
	6550 1500 6450 1500
Wire Wire Line
	6450 1600 6550 1600
Wire Wire Line
	6550 1700 6450 1700
Wire Wire Line
	6450 1800 6550 1800
Wire Wire Line
	6550 1900 6450 1900
Wire Wire Line
	6450 2000 6550 2000
Wire Wire Line
	6550 2100 6450 2100
Wire Wire Line
	6450 2200 6550 2200
Wire Wire Line
	6550 2300 6450 2300
Text GLabel 5800 3300 0    50   Input ~ 0
GND
Wire Wire Line
	5800 3300 5850 3300
Wire Wire Line
	5850 3300 5850 3200
Text GLabel 5650 600  0    50   Input ~ 0
VCC33
Wire Wire Line
	5850 600  5650 600 
Text GLabel 7300 1050 0    50   Input ~ 0
R16
Text GLabel 7300 1150 0    50   Input ~ 0
T16
Text GLabel 7300 1250 0    50   Input ~ 0
T15
Text GLabel 7300 1350 0    50   Input ~ 0
T14
Text GLabel 7300 1450 0    50   Input ~ 0
T13
Text GLabel 7300 1650 0    50   Input ~ 0
N12
Text GLabel 7300 1750 0    50   Input ~ 0
M11
Text GLabel 7300 1850 0    50   Input ~ 0
N10
Text GLabel 7300 1950 0    50   Input ~ 0
P10
Text GLabel 7300 2250 0    50   Input ~ 0
T11
Text GLabel 7300 2350 0    50   Input ~ 0
R10
Text GLabel 7300 2450 0    50   Input ~ 0
T10
Text GLabel 7300 2550 0    50   Input ~ 0
P9
Text GLabel 7300 2650 0    50   Input ~ 0
P8
Text GLabel 7300 2750 0    50   Input ~ 0
R9
Text GLabel 7300 2850 0    50   Input ~ 0
T9
Text GLabel 7300 2950 0    50   Input ~ 0
T8
Text GLabel 7300 3050 0    50   Input ~ 0
T7
Text GLabel 7300 3150 0    50   Input ~ 0
R6
Wire Wire Line
	7300 1050 7500 1050
Wire Wire Line
	7500 1150 7300 1150
Wire Wire Line
	7300 1250 7500 1250
Wire Wire Line
	7500 1350 7300 1350
Wire Wire Line
	7300 1450 7500 1450
Wire Wire Line
	7300 1650 7500 1650
Wire Wire Line
	7500 1750 7300 1750
Wire Wire Line
	7300 1850 7500 1850
Wire Wire Line
	7500 1950 7300 1950
Wire Wire Line
	7300 2250 7500 2250
Wire Wire Line
	7500 2350 7300 2350
Wire Wire Line
	7300 2450 7500 2450
Wire Wire Line
	7500 2550 7300 2550
Wire Wire Line
	7500 2650 7300 2650
Wire Wire Line
	7300 2750 7500 2750
Wire Wire Line
	7300 2850 7500 2850
Wire Wire Line
	7300 2950 7500 2950
Wire Wire Line
	7500 3050 7300 3050
Wire Wire Line
	7300 3150 7500 3150
Text GLabel 8200 1050 2    50   Input ~ 0
R1
Text GLabel 8200 1150 2    50   Input ~ 0
P1
Text GLabel 8200 1250 2    50   Input ~ 0
P2
Text GLabel 8200 1350 2    50   Input ~ 0
N3
Text GLabel 8200 1650 2    50   Input ~ 0
M1
Text GLabel 8200 1750 2    50   Input ~ 0
L3
Text GLabel 8200 1850 2    50   Input ~ 0
L1
Text GLabel 8200 1950 2    50   Input ~ 0
K3
Text GLabel 8200 2250 2    50   Input ~ 0
K1
Text GLabel 8200 2350 2    50   Input ~ 0
J2
Text GLabel 8200 2450 2    50   Input ~ 0
J1
Text GLabel 8200 2550 2    50   Input ~ 0
H2
Wire Wire Line
	8050 1050 8200 1050
Wire Wire Line
	8200 1150 8050 1150
Wire Wire Line
	8050 1250 8200 1250
Wire Wire Line
	8200 1350 8050 1350
Wire Wire Line
	8200 1650 8050 1650
Wire Wire Line
	8050 1750 8200 1750
Wire Wire Line
	8200 1850 8050 1850
Wire Wire Line
	8050 1950 8200 1950
Wire Wire Line
	8050 2250 8200 2250
Wire Wire Line
	8050 2350 8200 2350
Wire Wire Line
	8050 2450 8200 2450
Wire Wire Line
	8050 2550 8200 2550
Wire Wire Line
	8050 2750 8200 2750
Wire Wire Line
	8200 2850 8050 2850
Wire Wire Line
	8050 2950 8200 2950
Wire Wire Line
	8200 3050 8050 3050
Wire Wire Line
	8050 3150 8200 3150
Text GLabel 10800 6150 0    50   Input ~ 0
GND
$Comp
L power:GND #PWR0101
U 1 1 5E843433
P 10850 6300
F 0 "#PWR0101" H 10850 6050 50  0001 C CNN
F 1 "GND" H 10855 6127 50  0000 C CNN
F 2 "" H 10850 6300 50  0001 C CNN
F 3 "" H 10850 6300 50  0001 C CNN
	1    10850 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	10850 6300 10850 6150
Wire Wire Line
	10850 6150 10800 6150
Text GLabel 8200 2850 2    50   Input ~ 0
H1
Text GLabel 8200 2950 2    50   Input ~ 0
F2
Text GLabel 8200 3050 2    50   Input ~ 0
G1
Text GLabel 8200 3150 2    50   Input ~ 0
E2
Text GLabel 3950 1050 2    50   Input ~ 0
F1
Text GLabel 1700 750  2    50   Input ~ 0
VCCIO0
Text GLabel 800  1050 0    50   Input ~ 0
GND
Text GLabel 1700 1050 2    50   Input ~ 0
GND
Text GLabel 800  1450 0    50   Input ~ 0
GND
Text GLabel 1700 1450 2    50   Input ~ 0
GND
Text GLabel 800  1850 0    50   Input ~ 0
GND
Text GLabel 1700 1850 2    50   Input ~ 0
GND
Text GLabel 800  2250 0    50   Input ~ 0
GND
Text GLabel 1700 2250 2    50   Input ~ 0
GND
Text GLabel 800  2650 0    50   Input ~ 0
GND
Text GLabel 1700 2650 2    50   Input ~ 0
GND
Text GLabel 800  1150 0    50   Input ~ 0
B12
Text GLabel 800  1250 0    50   Input ~ 0
A11
Text GLabel 800  1350 0    50   Input ~ 0
A10
Text GLabel 800  1550 0    50   Input ~ 0
A9
Text GLabel 800  1650 0    50   Input ~ 0
B8
Text GLabel 800  1750 0    50   Input ~ 0
B7
Text GLabel 800  1950 0    50   Input ~ 0
A6
Text GLabel 800  2050 0    50   Input ~ 0
B6
Text GLabel 800  2150 0    50   Input ~ 0
A5
Text GLabel 800  2350 0    50   Input ~ 0
B5
Text GLabel 800  2450 0    50   Input ~ 0
B4
Text GLabel 800  2550 0    50   Input ~ 0
A2
Text GLabel 1700 1150 2    50   Input ~ 0
B11
Text GLabel 1700 1250 2    50   Input ~ 0
B10
Text GLabel 1700 1350 2    50   Input ~ 0
C9
Text GLabel 1700 1550 2    50   Input ~ 0
B9
Text GLabel 1700 1650 2    50   Input ~ 0
A7
Text GLabel 1700 1750 2    50   Input ~ 0
C7
Text GLabel 1700 1950 2    50   Input ~ 0
C6
Text GLabel 1700 2050 2    50   Input ~ 0
C5
Text GLabel 1700 2150 2    50   Input ~ 0
C4
Text GLabel 1700 2350 2    50   Input ~ 0
C3
Text GLabel 1700 2450 2    50   Input ~ 0
B3
Text GLabel 1700 2550 2    50   Input ~ 0
A1
Text GLabel 1700 4050 2    50   Input ~ 0
VCCIO1
Text GLabel 800  4350 0    50   Input ~ 0
GND
Text GLabel 1700 4350 2    50   Input ~ 0
GND
Text GLabel 800  4750 0    50   Input ~ 0
GND
Text GLabel 1700 4750 2    50   Input ~ 0
GND
Text GLabel 800  5150 0    50   Input ~ 0
GND
Text GLabel 1700 5150 2    50   Input ~ 0
GND
Text GLabel 800  5550 0    50   Input ~ 0
GND
Text GLabel 1700 5550 2    50   Input ~ 0
GND
Text GLabel 800  5950 0    50   Input ~ 0
GND
Text GLabel 1700 5950 2    50   Input ~ 0
GND
Text GLabel 800  4050 0    50   Input ~ 0
VCC12
Text GLabel 800  4250 0    50   Input ~ 0
P16
Text GLabel 800  4450 0    50   Input ~ 0
N16
Text GLabel 800  4550 0    50   Input ~ 0
M16
Text GLabel 800  4650 0    50   Input ~ 0
K15
Text GLabel 800  4850 0    50   Input ~ 0
K14
Text GLabel 800  4950 0    50   Input ~ 0
G14
Text GLabel 800  5050 0    50   Input ~ 0
J15
Text GLabel 800  5250 0    50   Input ~ 0
H16
Text GLabel 800  5350 0    50   Input ~ 0
G16
Text GLabel 800  5450 0    50   Input ~ 0
F16
Text GLabel 800  5650 0    50   Input ~ 0
E16
Text GLabel 800  5750 0    50   Input ~ 0
D16
Text GLabel 800  5850 0    50   Input ~ 0
C16
Text GLabel 1700 4150 2    50   Input ~ 0
R15
Text GLabel 1700 4250 2    50   Input ~ 0
P15
Text GLabel 1700 4450 2    50   Input ~ 0
M15
Text GLabel 1700 4550 2    50   Input ~ 0
L16
Text GLabel 1700 4650 2    50   Input ~ 0
K16
Text GLabel 1700 4850 2    50   Input ~ 0
J14
Text GLabel 1700 4950 2    50   Input ~ 0
F14
Text GLabel 1700 5050 2    50   Input ~ 0
H14
Text GLabel 1700 5250 2    50   Input ~ 0
G15
Text GLabel 1700 5350 2    50   Input ~ 0
F15
Text GLabel 1700 5450 2    50   Input ~ 0
E14
Text GLabel 1700 5650 2    50   Input ~ 0
D15
Text GLabel 1700 5750 2    50   Input ~ 0
D14
Text GLabel 1700 5850 2    50   Input ~ 0
B16
Wire Wire Line
	800  4050 900  4050
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J2
U 1 1 5EBF951A
P 1200 4950
F 0 "J2" H 1250 6067 50  0000 C CNN
F 1 "Conn_02x20_Odd_Even" H 1250 5976 50  0000 C CNN
F 2 "immobilizer_lib:PinHeader_2x20_P2.54mm_Vertical" H 1200 4950 50  0001 C CNN
F 3 "~" H 1200 4950 50  0001 C CNN
	1    1200 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	900  4050 900  4150
Wire Wire Line
	900  4150 1000 4150
Connection ~ 900  4050
Wire Wire Line
	900  4050 1000 4050
Text GLabel 3750 4000 0    50   Input ~ 0
SND_L
Text GLabel 3750 4100 0    50   Input ~ 0
SND_R
Text GLabel 3750 4250 0    50   Input ~ 0
VID_R0
Text GLabel 3750 4350 0    50   Input ~ 0
VID_R1
Text GLabel 3750 4450 0    50   Input ~ 0
VID_R2
Text GLabel 3750 4550 0    50   Input ~ 0
VID_R3
Text GLabel 3750 4650 0    50   Input ~ 0
VID_R4
Text GLabel 3750 4750 0    50   Input ~ 0
VID_R5
Text GLabel 3750 4900 0    50   Input ~ 0
VID_G0
Text GLabel 3750 5000 0    50   Input ~ 0
VID_G1
Text GLabel 3750 5100 0    50   Input ~ 0
VID_G2
Text GLabel 3750 5200 0    50   Input ~ 0
VID_G3
Text GLabel 3750 5300 0    50   Input ~ 0
VID_G4
Text GLabel 3750 5400 0    50   Input ~ 0
VID_G5
Text GLabel 3750 5550 0    50   Input ~ 0
VID_B0
Text GLabel 3750 5650 0    50   Input ~ 0
VID_B1
Text GLabel 3750 5750 0    50   Input ~ 0
VID_B2
Text GLabel 3750 5850 0    50   Input ~ 0
VID_B3
Text GLabel 3750 5950 0    50   Input ~ 0
VID_B4
Text GLabel 3750 6050 0    50   Input ~ 0
VID_B5
Text GLabel 3750 6250 0    50   Input ~ 0
VID_HS
Text GLabel 3750 6350 0    50   Input ~ 0
VID_VS
Text GLabel 2900 4000 2    50   Input ~ 0
SND_L
Text GLabel 2900 4100 2    50   Input ~ 0
SND_R
Text GLabel 2900 4250 2    50   Input ~ 0
VID_R0
Text GLabel 2900 4350 2    50   Input ~ 0
VID_R1
Text GLabel 2900 4450 2    50   Input ~ 0
VID_R2
Text GLabel 2900 4550 2    50   Input ~ 0
VID_R3
Text GLabel 2900 4650 2    50   Input ~ 0
VID_R4
Text GLabel 2900 4750 2    50   Input ~ 0
VID_R5
Text GLabel 2900 4900 2    50   Input ~ 0
VID_G0
Text GLabel 2900 5000 2    50   Input ~ 0
VID_G1
Text GLabel 2900 5100 2    50   Input ~ 0
VID_G2
Text GLabel 2900 5200 2    50   Input ~ 0
VID_G3
Text GLabel 2900 5300 2    50   Input ~ 0
VID_G4
Text GLabel 2900 5400 2    50   Input ~ 0
VID_G5
Text GLabel 2900 5550 2    50   Input ~ 0
VID_B0
Text GLabel 2900 5650 2    50   Input ~ 0
VID_B1
Text GLabel 2900 5750 2    50   Input ~ 0
VID_B2
Text GLabel 2900 5850 2    50   Input ~ 0
VID_B3
Text GLabel 2900 5950 2    50   Input ~ 0
VID_B4
Text GLabel 2900 6050 2    50   Input ~ 0
VID_B5
Text GLabel 2900 6250 2    50   Input ~ 0
VID_HS
Text GLabel 2900 6350 2    50   Input ~ 0
VID_VS
Wire Wire Line
	2750 4000 2900 4000
Wire Wire Line
	2750 4100 2900 4100
Wire Wire Line
	2750 4250 2900 4250
Wire Wire Line
	2750 4350 2900 4350
Wire Wire Line
	2750 4450 2900 4450
Wire Wire Line
	2750 4550 2900 4550
Wire Wire Line
	2750 4650 2900 4650
Wire Wire Line
	2750 4750 2900 4750
Wire Wire Line
	2750 4900 2900 4900
Wire Wire Line
	2750 5000 2900 5000
Wire Wire Line
	2750 5100 2900 5100
Wire Wire Line
	2750 5200 2900 5200
Wire Wire Line
	2750 5300 2900 5300
Wire Wire Line
	2750 5400 2900 5400
Wire Wire Line
	2750 5550 2900 5550
Wire Wire Line
	2750 5650 2900 5650
Wire Wire Line
	2750 5750 2900 5750
Wire Wire Line
	2750 5850 2900 5850
Wire Wire Line
	2750 5950 2900 5950
Wire Wire Line
	2750 6050 2900 6050
Wire Wire Line
	2750 6250 2900 6250
Wire Wire Line
	2750 6350 2900 6350
Text GLabel 2650 2850 0    50   Input ~ 0
B10
Text GLabel 2650 2050 0    50   Input ~ 0
C7
Text GLabel 2650 1850 0    50   Input ~ 0
C6
Text GLabel 2650 1650 0    50   Input ~ 0
C5
Text GLabel 2650 1450 0    50   Input ~ 0
C4
Text GLabel 2650 1250 0    50   Input ~ 0
C3
Text GLabel 2650 1050 0    50   Input ~ 0
B3
Text GLabel 2650 950  0    50   Input ~ 0
B4
Text GLabel 2650 1150 0    50   Input ~ 0
B5
Text GLabel 2650 1550 0    50   Input ~ 0
B6
Text GLabel 2650 1950 0    50   Input ~ 0
B7
Text GLabel 2650 2150 0    50   Input ~ 0
B8
Text GLabel 2650 850  0    50   Input ~ 0
A1
Text GLabel 2650 750  0    50   Input ~ 0
A2
Text GLabel 2750 4450 0    50   Input ~ 0
K16
Text GLabel 2750 5750 0    50   Input ~ 0
E14
Text GLabel 2750 5550 0    50   Input ~ 0
E16
Text GLabel 3950 750  2    50   Input ~ 0
T6
Text GLabel 3950 950  2    50   Input ~ 0
R5
Text GLabel 3950 1350 2    50   Input ~ 0
R3
Text GLabel 3950 1450 2    50   Input ~ 0
R4
Text GLabel 3950 1650 2    50   Input ~ 0
R2
Text GLabel 3950 1750 2    50   Input ~ 0
T3
Text GLabel 3950 1950 2    50   Input ~ 0
T1
Text GLabel 3950 2050 2    50   Input ~ 0
T2
Text GLabel 3950 850  2    50   Input ~ 0
T5
Text GLabel 8200 2750 2    50   Input ~ 0
G2
Text GLabel 3950 1250 2    50   Input ~ 0
D1
Text GLabel 3950 1150 2    50   Input ~ 0
D2
Text GLabel 3950 1850 2    50   Input ~ 0
C1
Text GLabel 3950 1550 2    50   Input ~ 0
C2
Text GLabel 3950 2150 2    50   Input ~ 0
B1
Text GLabel 3950 2250 2    50   Input ~ 0
B2
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J1
U 1 1 5EB51D03
P 1200 1650
F 0 "J1" H 1250 2767 50  0000 C CNN
F 1 "Conn_02x20_Odd_Even" H 1250 2676 50  0000 C CNN
F 2 "immobilizer_lib:PinHeader_2x20_P2.54mm_Vertical" H 1200 1650 50  0001 C CNN
F 3 "~" H 1200 1650 50  0001 C CNN
	1    1200 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	800  1050 1000 1050
Wire Wire Line
	800  1850 1000 1850
Wire Wire Line
	800  750  1000 750 
Wire Wire Line
	800  850  1000 850 
Wire Wire Line
	800  950  1000 950 
Wire Wire Line
	800  1150 1000 1150
Wire Wire Line
	800  1250 1000 1250
Wire Wire Line
	800  1350 1000 1350
Wire Wire Line
	800  1550 1000 1550
Wire Wire Line
	800  1650 1000 1650
Wire Wire Line
	800  1750 1000 1750
Wire Wire Line
	800  2050 1000 2050
Wire Wire Line
	800  2150 1000 2150
Wire Wire Line
	800  2550 1000 2550
Wire Wire Line
	800  2450 1000 2450
Wire Wire Line
	800  2350 1000 2350
Wire Wire Line
	800  2650 1000 2650
Wire Wire Line
	800  2250 1000 2250
Wire Wire Line
	800  1950 1000 1950
Wire Wire Line
	800  1450 1000 1450
Wire Wire Line
	8850 1350 9100 1350
Wire Wire Line
	8850 1750 9100 1750
Wire Wire Line
	8850 2150 9100 2150
Wire Wire Line
	8850 1050 9100 1050
Wire Wire Line
	8850 1150 9100 1150
Wire Wire Line
	8850 1250 9100 1250
Wire Wire Line
	8850 1450 9100 1450
Wire Wire Line
	8850 1550 9100 1550
Wire Wire Line
	8850 1650 9100 1650
Wire Wire Line
	8850 1850 9100 1850
Wire Wire Line
	8850 1950 9100 1950
Wire Wire Line
	8850 2050 9100 2050
Wire Wire Line
	8850 2250 9100 2250
Wire Wire Line
	8850 2350 9100 2350
Wire Wire Line
	8850 2450 9100 2450
Wire Wire Line
	8850 2850 9100 2850
Wire Wire Line
	8850 2750 9100 2750
Wire Wire Line
	8850 2650 9100 2650
Wire Wire Line
	8850 2950 9100 2950
Wire Wire Line
	8850 2550 9100 2550
Wire Wire Line
	6450 4000 6550 4000
Wire Wire Line
	6400 5900 6650 5900
Wire Wire Line
	6400 5500 6650 5500
Wire Wire Line
	6400 5100 6650 5100
Wire Wire Line
	6400 4700 6650 4700
Wire Wire Line
	6400 4300 6650 4300
Wire Wire Line
	6400 4200 6650 4200
Wire Wire Line
	6400 4400 6650 4400
Wire Wire Line
	6400 4500 6650 4500
Wire Wire Line
	6400 4600 6650 4600
Wire Wire Line
	6400 4800 6650 4800
Wire Wire Line
	6400 4900 6650 4900
Wire Wire Line
	6400 5000 6650 5000
Wire Wire Line
	6400 5200 6650 5200
Wire Wire Line
	6400 5300 6650 5300
Wire Wire Line
	6400 5400 6650 5400
Wire Wire Line
	6400 5600 6650 5600
Wire Wire Line
	6400 5700 6650 5700
Wire Wire Line
	6400 5800 6650 5800
Wire Wire Line
	800  4350 1000 4350
Wire Wire Line
	800  4750 1000 4750
Wire Wire Line
	800  5150 1000 5150
Wire Wire Line
	800  4250 1000 4250
Wire Wire Line
	800  4450 1000 4450
Wire Wire Line
	800  4550 1000 4550
Wire Wire Line
	800  4650 1000 4650
Wire Wire Line
	800  4850 1000 4850
Wire Wire Line
	800  4950 1000 4950
Wire Wire Line
	800  5050 1000 5050
Wire Wire Line
	800  5250 1000 5250
Wire Wire Line
	800  5350 1000 5350
Wire Wire Line
	800  5450 1000 5450
Wire Wire Line
	800  5850 1000 5850
Wire Wire Line
	800  5750 1000 5750
Wire Wire Line
	800  5650 1000 5650
Wire Wire Line
	800  5950 1000 5950
Wire Wire Line
	800  5550 1000 5550
Wire Wire Line
	1500 5150 1700 5150
Wire Wire Line
	1500 4750 1700 4750
Wire Wire Line
	1500 4350 1700 4350
Wire Wire Line
	1500 4050 1700 4050
Wire Wire Line
	1500 4150 1700 4150
Wire Wire Line
	1500 4250 1700 4250
Wire Wire Line
	1500 4450 1700 4450
Wire Wire Line
	1500 4550 1700 4550
Wire Wire Line
	1500 4650 1700 4650
Wire Wire Line
	1500 4850 1700 4850
Wire Wire Line
	1500 4950 1700 4950
Wire Wire Line
	1500 5050 1700 5050
Wire Wire Line
	1500 5250 1700 5250
Wire Wire Line
	1500 5350 1700 5350
Wire Wire Line
	1500 5450 1700 5450
Wire Wire Line
	1500 5850 1700 5850
Wire Wire Line
	1500 5750 1700 5750
Wire Wire Line
	1500 5650 1700 5650
Wire Wire Line
	1500 5550 1700 5550
Wire Wire Line
	1500 5950 1700 5950
Wire Wire Line
	1500 1850 1700 1850
Wire Wire Line
	1500 1050 1700 1050
Wire Wire Line
	1500 750  1700 750 
Wire Wire Line
	1500 850  1700 850 
Wire Wire Line
	1500 950  1700 950 
Wire Wire Line
	1500 1150 1700 1150
Wire Wire Line
	1500 1250 1700 1250
Wire Wire Line
	1500 1350 1700 1350
Wire Wire Line
	1500 1550 1700 1550
Wire Wire Line
	1500 1650 1700 1650
Wire Wire Line
	1500 1750 1700 1750
Wire Wire Line
	1500 2050 1700 2050
Wire Wire Line
	1500 2150 1700 2150
Wire Wire Line
	1500 2550 1700 2550
Wire Wire Line
	1500 2450 1700 2450
Wire Wire Line
	1500 2350 1700 2350
Wire Wire Line
	1500 2250 1700 2250
Wire Wire Line
	1500 2650 1700 2650
Wire Wire Line
	1500 1450 1700 1450
Wire Wire Line
	1500 1950 1700 1950
Wire Wire Line
	9600 2150 9750 2150
Wire Wire Line
	9600 1750 9750 1750
Wire Wire Line
	9600 1350 9750 1350
Wire Wire Line
	9600 1050 9750 1050
Wire Wire Line
	9600 1150 9750 1150
Wire Wire Line
	9600 1250 9750 1250
Wire Wire Line
	9600 1450 9750 1450
Wire Wire Line
	9600 1550 9750 1550
Wire Wire Line
	9600 1650 9750 1650
Wire Wire Line
	9600 1850 9750 1850
Wire Wire Line
	9600 1950 9750 1950
Wire Wire Line
	9600 2050 9750 2050
Wire Wire Line
	9600 2250 9750 2250
Wire Wire Line
	9600 2350 9750 2350
Wire Wire Line
	9600 2450 9750 2450
Wire Wire Line
	9600 2850 9750 2850
Wire Wire Line
	9600 2750 9750 2750
Wire Wire Line
	9600 2650 9750 2650
Wire Wire Line
	9600 2550 9750 2550
Wire Wire Line
	9600 2950 9750 2950
Wire Wire Line
	7150 4000 7300 4000
Wire Wire Line
	7150 5900 7300 5900
Wire Wire Line
	7150 5500 7300 5500
Wire Wire Line
	7150 5100 7300 5100
Wire Wire Line
	7150 4700 7300 4700
Wire Wire Line
	7150 4300 7300 4300
Wire Wire Line
	7150 5800 7300 5800
Wire Wire Line
	7150 5700 7300 5700
Wire Wire Line
	7150 5600 7300 5600
Wire Wire Line
	7150 5400 7300 5400
Wire Wire Line
	7150 5300 7300 5300
Wire Wire Line
	7150 5200 7300 5200
Wire Wire Line
	7150 5000 7300 5000
Wire Wire Line
	7150 4900 7300 4900
Wire Wire Line
	7150 4800 7300 4800
Wire Wire Line
	7150 4600 7300 4600
Wire Wire Line
	7150 4500 7300 4500
Wire Wire Line
	7150 4400 7300 4400
Wire Wire Line
	7150 4200 7300 4200
Wire Wire Line
	7150 4100 7300 4100
$Sheet
S 2750 650  1100 2600
U 5E8ACECD
F0 "Console Interface" 50
F1 "Console_Interface.sch" 50
F2 "LCD_IN_00" I L 2750 750 50 
F3 "LCD_IN_01" I L 2750 850 50 
F4 "LCD_IN_02" I L 2750 950 50 
F5 "LCD_IN_03" I L 2750 1050 50 
F6 "LCD_IN_04" I L 2750 1150 50 
F7 "LCD_IN_05" I L 2750 1250 50 
F8 "LCD_IN_06" I L 2750 1350 50 
F9 "LCD_IN_07" I L 2750 1450 50 
F10 "LCD_IN_08" I L 2750 1550 50 
F11 "LCD_IN_09" I L 2750 1650 50 
F12 "LCD_IN_10" I L 2750 1750 50 
F13 "LCD_IN_11" I L 2750 1850 50 
F14 "LCD_IN_12" I L 2750 1950 50 
F15 "LCD_IN_13" I L 2750 2050 50 
F16 "LCD_IN_14" I L 2750 2150 50 
F17 "LCD_IN_15" I L 2750 2250 50 
F18 "LCD_IN_16" I L 2750 2350 50 
F19 "LCD_IN_17" I L 2750 2450 50 
F20 "LCD_IN_18" I L 2750 2550 50 
F21 "LCD_IN_19" I L 2750 2650 50 
F22 "LCD_IN_20" I L 2750 2750 50 
F23 "LCD_IN_21" I L 2750 2850 50 
F24 "CNT_OUT_00" I R 3850 750 50 
F25 "CNT_OUT_01" I R 3850 850 50 
F26 "CNT_OUT_02" I R 3850 950 50 
F27 "CNT_OUT_03" I R 3850 1050 50 
F28 "CNT_OUT_04" I R 3850 1150 50 
F29 "CNT_OUT_05" I R 3850 1250 50 
F30 "CNT_OUT_06" I R 3850 1350 50 
F31 "CNT_OUT_07" I R 3850 1450 50 
F32 "CNT_OUT_08" I R 3850 1550 50 
F33 "CNT_OUT_09" I R 3850 1650 50 
F34 "CNT_OUT_10" I R 3850 1750 50 
F35 "CNT_OUT_11" I R 3850 1850 50 
F36 "CNT_OUT_12" I R 3850 1950 50 
F37 "CNT_OUT_13" I R 3850 2050 50 
F38 "CNT_OUT_14" I R 3850 2150 50 
F39 "CNT_OUT_15" I R 3850 2250 50 
F40 "PAD_IN_1" I R 3850 2450 50 
F41 "PAD_IN_2" I R 3850 2550 50 
F42 "PAD_IN_3" I R 3850 2650 50 
F43 "PAD_IN_4" I R 3850 2750 50 
F44 "PAD_IN_6" I R 3850 2850 50 
F45 "PAD_IN_9" I R 3850 2950 50 
F46 "PAD_SEL_5" I R 3850 3150 50 
$EndSheet
Wire Wire Line
	3850 750  3950 750 
Wire Wire Line
	3850 850  3950 850 
Wire Wire Line
	3850 950  3950 950 
Wire Wire Line
	3850 1050 3950 1050
Wire Wire Line
	3850 1150 3950 1150
Wire Wire Line
	3850 1250 3950 1250
Wire Wire Line
	3850 1350 3950 1350
Wire Wire Line
	3850 1450 3950 1450
Wire Wire Line
	3850 1550 3950 1550
Wire Wire Line
	3850 1650 3950 1650
Wire Wire Line
	3850 1750 3950 1750
Wire Wire Line
	3850 1850 3950 1850
Wire Wire Line
	3850 1950 3950 1950
Wire Wire Line
	3850 2050 3950 2050
Wire Wire Line
	3850 2150 3950 2150
Wire Wire Line
	3850 2250 3950 2250
Wire Wire Line
	2650 750  2750 750 
Wire Wire Line
	2650 850  2750 850 
Wire Wire Line
	2650 950  2750 950 
Wire Wire Line
	2650 1050 2750 1050
Wire Wire Line
	2650 1150 2750 1150
Wire Wire Line
	2650 1250 2750 1250
Wire Wire Line
	2650 1350 2750 1350
Wire Wire Line
	2650 1450 2750 1450
Wire Wire Line
	2650 1550 2750 1550
Wire Wire Line
	2650 1650 2750 1650
Wire Wire Line
	2650 1750 2750 1750
Wire Wire Line
	2650 1850 2750 1850
Wire Wire Line
	2650 1950 2750 1950
Wire Wire Line
	2650 2050 2750 2050
Wire Wire Line
	2650 2150 2750 2150
Wire Wire Line
	2650 2250 2750 2250
Wire Wire Line
	2650 2350 2750 2350
Wire Wire Line
	2650 2450 2750 2450
Wire Wire Line
	2650 2550 2750 2550
Wire Wire Line
	2650 2650 2750 2650
Wire Wire Line
	2650 2750 2750 2750
Wire Wire Line
	2650 2850 2750 2850
Wire Wire Line
	3850 2450 3950 2450
Wire Wire Line
	3850 2550 3950 2550
Wire Wire Line
	3850 2650 3950 2650
Wire Wire Line
	3850 2750 3950 2750
Wire Wire Line
	3850 2850 3950 2850
Text GLabel 2650 1350 0    50   Input ~ 0
A5
Text GLabel 2650 1750 0    50   Input ~ 0
A6
Wire Wire Line
	3850 2950 3950 2950
Wire Wire Line
	3850 3150 3950 3150
Text GLabel 2650 2250 0    50   Input ~ 0
A7
Text GLabel 2650 2450 0    50   Input ~ 0
B9
Text GLabel 2650 2350 0    50   Input ~ 0
A9
Text GLabel 2650 2650 0    50   Input ~ 0
C9
Text GLabel 2650 2550 0    50   Input ~ 0
A10
Text GLabel 2650 2750 0    50   Input ~ 0
A11
Text GLabel 3950 2450 2    50   Input ~ 0
B11
Text GLabel 3950 3150 2    50   Input ~ 0
B12
Text GLabel 2750 4100 0    50   Input ~ 0
C16
Text GLabel 2750 4000 0    50   Input ~ 0
B16
Text GLabel 3950 2950 2    50   Input ~ 0
R15
Text GLabel 3950 2750 2    50   Input ~ 0
P16
Text GLabel 3950 2850 2    50   Input ~ 0
P15
Text GLabel 3950 2550 2    50   Input ~ 0
M15
Text GLabel 3950 2650 2    50   Input ~ 0
N16
Text GLabel 2750 4650 0    50   Input ~ 0
L16
Text GLabel 2750 4750 0    50   Input ~ 0
M16
Text GLabel 2750 4550 0    50   Input ~ 0
K15
Text GLabel 2750 4350 0    50   Input ~ 0
J14
Text GLabel 2750 4250 0    50   Input ~ 0
K14
Text GLabel 2750 5400 0    50   Input ~ 0
F14
Text GLabel 2750 5300 0    50   Input ~ 0
G14
Text GLabel 2750 5200 0    50   Input ~ 0
H14
Text GLabel 2750 5100 0    50   Input ~ 0
J15
Text GLabel 2750 5000 0    50   Input ~ 0
G15
Text GLabel 2750 4900 0    50   Input ~ 0
H16
Text GLabel 2750 6050 0    50   Input ~ 0
F15
Text GLabel 9750 5450 2    50   Input ~ 0
VCCIO2
Text GLabel 9750 5550 2    50   Input ~ 0
VCCIO3
Text GLabel 9750 5350 2    50   Input ~ 0
VCCIO1
Text GLabel 9750 5250 2    50   Input ~ 0
VCCIO0
Wire Wire Line
	9750 5250 9650 5250
Wire Wire Line
	9650 5250 9650 5350
Wire Wire Line
	9650 5350 9750 5350
Wire Wire Line
	9650 5350 9650 5450
Wire Wire Line
	9650 5550 9750 5550
Connection ~ 9650 5350
Wire Wire Line
	9750 5450 9650 5450
Connection ~ 9650 5450
Wire Wire Line
	9650 5450 9650 5550
Wire Wire Line
	9650 5350 9450 5350
Text GLabel 9450 5350 0    50   Input ~ 0
VCC33
$Comp
L Connector:USB_A J8
U 1 1 5E9FE611
P 5100 7050
F 0 "J8" H 5157 7517 50  0000 C CNN
F 1 "USB_A" H 5157 7426 50  0000 C CNN
F 2 "digikey/digikey-kicad-library/digikey-footprints.pretty:USB_A_Female_UE27AC54100" H 5250 7000 50  0001 C CNN
F 3 " ~" H 5250 7000 50  0001 C CNN
	1    5100 7050
	1    0    0    -1  
$EndComp
$Comp
L Connector:USB_B_Micro J7
U 1 1 5EA00BAC
P 5100 6000
F 0 "J7" H 5157 6467 50  0000 C CNN
F 1 "USB_B_Micro" H 5157 6376 50  0000 C CNN
F 2 "digikey/digikey-kicad-library/digikey-footprints.pretty:USB_Micro_B_Female_10118194-0001LF" H 5250 5950 50  0001 C CNN
F 3 "~" H 5250 5950 50  0001 C CNN
	1    5100 6000
	1    0    0    -1  
$EndComp
Text GLabel 2750 6250 0    50   Input ~ 0
D16
Text GLabel 2750 6350 0    50   Input ~ 0
D14
Text GLabel 2750 5850 0    50   Input ~ 0
F16
Text GLabel 2750 5650 0    50   Input ~ 0
D15
Text GLabel 2750 5950 0    50   Input ~ 0
G16
NoConn ~ 800  750 
NoConn ~ 800  850 
NoConn ~ 800  950 
NoConn ~ 1700 850 
NoConn ~ 1700 950 
Wire Wire Line
	5150 2950 5250 2950
Wire Wire Line
	5150 3050 5250 3050
Wire Wire Line
	5150 2650 5250 2650
Wire Wire Line
	7500 1550 7300 1550
Text GLabel 7500 1550 2    50   Input ~ 0
~CE
Wire Wire Line
	8050 1550 8200 1550
Wire Wire Line
	8050 1450 8200 1450
Text GLabel 8050 1450 0    50   Input ~ 0
~UB
Text GLabel 8050 1550 0    50   Input ~ 0
~LB
Text GLabel 5150 2650 0    50   Input ~ 0
~CE
Text GLabel 5150 3050 0    50   Input ~ 0
~UB
Text GLabel 5150 2950 0    50   Input ~ 0
~LB
Text GLabel 8200 1450 2    50   Input ~ 0
GND
Text GLabel 8200 1550 2    50   Input ~ 0
GND
Text GLabel 7300 1550 0    50   Input ~ 0
GND
Wire Wire Line
	5400 6000 5850 6000
Wire Wire Line
	5850 6000 5850 7050
Wire Wire Line
	5850 7050 5400 7050
Wire Wire Line
	5400 7150 5750 7150
Wire Wire Line
	5750 7150 5750 6100
Wire Wire Line
	5750 6100 5400 6100
Wire Wire Line
	5400 5800 5650 5800
Wire Wire Line
	5000 7450 5100 7450
Text GLabel 5000 7550 0    50   Input ~ 0
GND
Wire Wire Line
	5000 7450 5000 7550
Connection ~ 5000 7450
Wire Wire Line
	5000 6400 5100 6400
Wire Wire Line
	5000 6400 5000 6450
Connection ~ 5000 6400
Text GLabel 5000 6450 0    50   Input ~ 0
GND
$Comp
L power:+5V #PWR0102
U 1 1 5F4A436A
P 5650 5800
F 0 "#PWR0102" H 5650 5650 50  0001 C CNN
F 1 "+5V" H 5665 5973 50  0000 C CNN
F 2 "" H 5650 5800 50  0001 C CNN
F 3 "" H 5650 5800 50  0001 C CNN
	1    5650 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 6850 5400 6850
Connection ~ 5650 5800
Wire Wire Line
	5650 5800 5650 6850
Wire Wire Line
	9500 6400 9650 6400
Wire Wire Line
	9500 6100 9650 6100
$Comp
L Device:C C1
U 1 1 5E843363
P 9650 6250
F 0 "C1" H 9742 6296 50  0000 L CNN
F 1 "100nF" H 9742 6205 50  0000 L CNN
F 2 "KiCad/kicad-footprints/Capacitor_SMD.pretty:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 9650 6250 50  0001 C CNN
F 3 "~" H 9650 6250 50  0001 C CNN
	1    9650 6250
	1    0    0    -1  
$EndComp
Connection ~ 9650 6100
Connection ~ 9650 6400
Wire Wire Line
	9650 6400 10100 6400
$EndSCHEMATC
