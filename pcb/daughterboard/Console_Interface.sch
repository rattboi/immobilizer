EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 3
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L immobilizer_sym:SN74ALVC164245DL(SSOP48) U5
U 1 1 5E8BB146
P 5650 1900
F 0 "U5" H 5650 2687 60  0000 C CNN
F 1 "SN74ALVC164245" H 5650 2581 60  0000 C CNN
F 2 "KiCad/kicad-footprints/Package_SO.pretty:TSSOP-48_6.1x12.5mm_P0.5mm" H 5350 1900 60  0001 C CNN
F 3 "" H 5350 1900 60  0000 C CNN
	1    5650 1900
	1    0    0    -1  
$EndComp
$Comp
L immobilizer_sym:SN74ALVC164245DL(SSOP48) U5
U 2 1 5E8BB14C
P 5650 3100
F 0 "U5" H 5650 2450 60  0000 C CNN
F 1 "SN74ALVC164245" H 5650 2350 60  0000 C CNN
F 2 "KiCad/kicad-footprints/Package_SO.pretty:TSSOP-48_6.1x12.5mm_P0.5mm" H 5350 3100 60  0001 C CNN
F 3 "" H 5350 3100 60  0000 C CNN
	2    5650 3100
	1    0    0    -1  
$EndComp
$Comp
L immobilizer_sym:SN74ALVC164245DL(SSOP48) U3
U 3 1 5E8BB152
P 1950 4500
F 0 "U3" H 2150 4850 60  0000 L CNN
F 1 "SN74ALVC164245" H 1650 4950 60  0000 L CNN
F 2 "KiCad/kicad-footprints/Package_SO.pretty:TSSOP-48_6.1x12.5mm_P0.5mm" H 1650 4500 60  0001 C CNN
F 3 "" H 1650 4500 60  0000 C CNN
	3    1950 4500
	1    0    0    -1  
$EndComp
$Comp
L immobilizer_sym:SN74ALVC164245DL(SSOP48) U3
U 4 1 5E8BB158
P 1950 5600
F 0 "U3" H 2150 5250 60  0000 L CNN
F 1 "SN74ALVC164245" H 1600 5150 60  0000 L CNN
F 2 "KiCad/kicad-footprints/Package_SO.pretty:TSSOP-48_6.1x12.5mm_P0.5mm" H 1650 5600 60  0001 C CNN
F 3 "" H 1650 5600 60  0000 C CNN
	4    1950 5600
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J6
U 1 1 5F6A8042
P 9500 4250
F 0 "J6" H 9550 5367 50  0000 C CNN
F 1 "Conn_02x20_Odd_Even" H 9550 5276 50  0000 C CNN
F 2 "OLIMEX/KiCAD/KiCAD_Footprints/OLIMEX_Connectors-FP.pretty:HN2x20" H 9500 4250 50  0001 C CNN
F 3 "~" H 9500 4250 50  0001 C CNN
	1    9500 4250
	1    0    0    -1  
$EndComp
Text GLabel 9300 3450 0    50   Input ~ 0
CONS_O_15
Text GLabel 9300 3550 0    50   Input ~ 0
CONS_O_13
Text GLabel 9300 3650 0    50   Input ~ 0
CONS_O_11
Text GLabel 9300 3750 0    50   Input ~ 0
CONS_O_09
Text GLabel 9300 3850 0    50   Input ~ 0
CONS_O_07
Text GLabel 9300 3950 0    50   Input ~ 0
CONS_O_05
Text GLabel 9300 4050 0    50   Input ~ 0
CONS_O_03
Text GLabel 9300 4150 0    50   Input ~ 0
CONS_O_01
Text GLabel 9800 3450 2    50   Input ~ 0
CONS_O_14
Text GLabel 9800 3550 2    50   Input ~ 0
CONS_O_12
Text GLabel 9800 3650 2    50   Input ~ 0
CONS_O_10
Text GLabel 9800 3750 2    50   Input ~ 0
CONS_O_08
Text GLabel 9800 3850 2    50   Input ~ 0
CONS_O_06
Text GLabel 9800 3950 2    50   Input ~ 0
CONS_O_04
Text GLabel 9800 4050 2    50   Input ~ 0
CONS_O_02
Text GLabel 9800 4150 2    50   Input ~ 0
CONS_O_00
Text GLabel 5950 3500 2    50   Input ~ 0
CONS_O_14
Text GLabel 5950 3300 2    50   Input ~ 0
CONS_O_12
Text GLabel 5950 3100 2    50   Input ~ 0
CONS_O_10
Text GLabel 5950 2900 2    50   Input ~ 0
CONS_O_08
Text GLabel 5950 2300 2    50   Input ~ 0
CONS_O_06
Text GLabel 5950 2100 2    50   Input ~ 0
CONS_O_04
Text GLabel 5950 1900 2    50   Input ~ 0
CONS_O_02
Text GLabel 5950 1700 2    50   Input ~ 0
CONS_O_00
Text GLabel 5950 3600 2    50   Input ~ 0
CONS_O_15
Text GLabel 5950 3400 2    50   Input ~ 0
CONS_O_13
Text GLabel 5950 3200 2    50   Input ~ 0
CONS_O_11
Text GLabel 5950 3000 2    50   Input ~ 0
CONS_O_09
Text GLabel 5950 2400 2    50   Input ~ 0
CONS_O_07
Text GLabel 5950 2200 2    50   Input ~ 0
CONS_O_05
Text GLabel 5950 2000 2    50   Input ~ 0
CONS_O_03
Text GLabel 5950 1800 2    50   Input ~ 0
CONS_O_01
Text GLabel 9300 3350 0    50   Input ~ 0
GND
Text GLabel 9800 3350 2    50   Input ~ 0
CONS_VCC
$Comp
L immobilizer_sym:SN74ALVC164245DL(SSOP48) U4
U 3 1 5F6F97B2
P 3750 4500
F 0 "U4" H 3950 4850 60  0000 L CNN
F 1 "SN74ALVC164245" H 3450 4950 60  0000 L CNN
F 2 "KiCad/kicad-footprints/Package_SO.pretty:TSSOP-48_6.1x12.5mm_P0.5mm" H 3450 4500 60  0001 C CNN
F 3 "" H 3450 4500 60  0000 C CNN
	3    3750 4500
	1    0    0    -1  
$EndComp
$Comp
L immobilizer_sym:SN74ALVC164245DL(SSOP48) U4
U 4 1 5F6F97B8
P 3750 5600
F 0 "U4" H 3950 5250 60  0000 L CNN
F 1 "SN74ALVC164245" H 3400 5150 60  0000 L CNN
F 2 "KiCad/kicad-footprints/Package_SO.pretty:TSSOP-48_6.1x12.5mm_P0.5mm" H 3450 5600 60  0001 C CNN
F 3 "" H 3450 5600 60  0000 C CNN
	4    3750 5600
	1    0    0    -1  
$EndComp
$Comp
L immobilizer_sym:SN74ALVC164245DL(SSOP48) U5
U 4 1 5F701DF9
P 5650 5600
F 0 "U5" H 5850 5250 60  0000 L CNN
F 1 "SN74ALVC164245" H 5300 5150 60  0000 L CNN
F 2 "KiCad/kicad-footprints/Package_SO.pretty:TSSOP-48_6.1x12.5mm_P0.5mm" H 5350 5600 60  0001 C CNN
F 3 "" H 5350 5600 60  0000 C CNN
	4    5650 5600
	1    0    0    -1  
$EndComp
$Comp
L immobilizer_sym:SN74ALVC164245DL(SSOP48) U4
U 1 1 5F71D32A
P 3750 1850
F 0 "U4" H 3750 2637 60  0000 C CNN
F 1 "SN74ALVC164245" H 3750 2531 60  0000 C CNN
F 2 "KiCad/kicad-footprints/Package_SO.pretty:TSSOP-48_6.1x12.5mm_P0.5mm" H 3450 1850 60  0001 C CNN
F 3 "" H 3450 1850 60  0000 C CNN
	1    3750 1850
	1    0    0    -1  
$EndComp
$Comp
L immobilizer_sym:SN74ALVC164245DL(SSOP48) U4
U 2 1 5F71D330
P 3750 3050
F 0 "U4" H 3750 2400 60  0000 C CNN
F 1 "SN74ALVC164245" H 3750 2300 60  0000 C CNN
F 2 "KiCad/kicad-footprints/Package_SO.pretty:TSSOP-48_6.1x12.5mm_P0.5mm" H 3450 3050 60  0001 C CNN
F 3 "" H 3450 3050 60  0000 C CNN
	2    3750 3050
	1    0    0    -1  
$EndComp
$Comp
L immobilizer_sym:SN74ALVC164245DL(SSOP48) U3
U 1 1 5F7364F1
P 1950 1850
F 0 "U3" H 1950 2637 60  0000 C CNN
F 1 "SN74ALVC164245" H 1950 2531 60  0000 C CNN
F 2 "KiCad/kicad-footprints/Package_SO.pretty:TSSOP-48_6.1x12.5mm_P0.5mm" H 1800 3250 60  0001 C CNN
F 3 "" H 1650 1850 60  0000 C CNN
	1    1950 1850
	1    0    0    -1  
$EndComp
$Comp
L immobilizer_sym:SN74ALVC164245DL(SSOP48) U3
U 2 1 5F7364F7
P 1950 3050
F 0 "U3" H 1950 2400 60  0000 C CNN
F 1 "SN74ALVC164245" H 1950 2300 60  0000 C CNN
F 2 "KiCad/kicad-footprints/Package_SO.pretty:TSSOP-48_6.1x12.5mm_P0.5mm" H 1650 3050 60  0001 C CNN
F 3 "" H 1650 3050 60  0000 C CNN
	2    1950 3050
	1    0    0    -1  
$EndComp
Text GLabel 9300 4950 0    50   Input ~ 0
CONS_I_14
Text GLabel 9300 4850 0    50   Input ~ 0
CONS_I_12
Text GLabel 9300 4750 0    50   Input ~ 0
CONS_I_10
Text GLabel 9300 4650 0    50   Input ~ 0
CONS_I_08
Text GLabel 9300 4550 0    50   Input ~ 0
CONS_I_06
Text GLabel 9300 4450 0    50   Input ~ 0
CONS_I_04
Text GLabel 9300 4350 0    50   Input ~ 0
CONS_I_02
Text GLabel 9300 4250 0    50   Input ~ 0
CONS_I_00
Text GLabel 9300 5050 0    50   Input ~ 0
CONS_I_16
Text GLabel 9300 5150 0    50   Input ~ 0
CONS_I_18
Text GLabel 9300 5250 0    50   Input ~ 0
CONS_I_20
Text GLabel 9800 4950 2    50   Input ~ 0
CONS_I_15
Text GLabel 9800 4850 2    50   Input ~ 0
CONS_I_13
Text GLabel 9800 4750 2    50   Input ~ 0
CONS_I_11
Text GLabel 9800 4650 2    50   Input ~ 0
CONS_I_09
Text GLabel 9800 4550 2    50   Input ~ 0
CONS_I_07
Text GLabel 9800 4450 2    50   Input ~ 0
CONS_I_05
Text GLabel 9800 4350 2    50   Input ~ 0
CONS_I_03
Text GLabel 9800 4250 2    50   Input ~ 0
CONS_I_01
Text GLabel 9800 5050 2    50   Input ~ 0
CONS_I_17
Text GLabel 9800 5150 2    50   Input ~ 0
CONS_I_19
Text GLabel 9800 5250 2    50   Input ~ 0
CONS_I_21
Text GLabel 2250 3550 2    50   Input ~ 0
CONS_I_07
Text GLabel 2250 3450 2    50   Input ~ 0
CONS_I_06
Text GLabel 2250 3350 2    50   Input ~ 0
CONS_I_05
Text GLabel 2250 3250 2    50   Input ~ 0
CONS_I_04
Text GLabel 2250 3150 2    50   Input ~ 0
CONS_I_03
Text GLabel 2250 3050 2    50   Input ~ 0
CONS_I_02
Text GLabel 2250 2950 2    50   Input ~ 0
CONS_I_01
Text GLabel 2250 2850 2    50   Input ~ 0
CONS_I_00
Text GLabel 4050 2350 2    50   Input ~ 0
CONS_I_15
Text GLabel 4050 2250 2    50   Input ~ 0
CONS_I_14
Text GLabel 4050 2150 2    50   Input ~ 0
CONS_I_13
Text GLabel 4050 2050 2    50   Input ~ 0
CONS_I_12
Text GLabel 4050 1950 2    50   Input ~ 0
CONS_I_11
Text GLabel 4050 1850 2    50   Input ~ 0
CONS_I_10
Text GLabel 4050 1750 2    50   Input ~ 0
CONS_I_09
Text GLabel 4050 1650 2    50   Input ~ 0
CONS_I_08
Text GLabel 4050 3350 2    50   Input ~ 0
CONS_I_21
Text GLabel 4050 3250 2    50   Input ~ 0
CONS_I_20
Text GLabel 4050 3150 2    50   Input ~ 0
CONS_I_19
Text GLabel 4050 3050 2    50   Input ~ 0
CONS_I_18
Text GLabel 4050 2950 2    50   Input ~ 0
CONS_I_17
Text GLabel 4050 2850 2    50   Input ~ 0
CONS_I_16
NoConn ~ 4050 3450
NoConn ~ 4050 3550
NoConn ~ 2250 2250
NoConn ~ 2250 2350
Text HLabel 1650 2850 0    50   Input ~ 0
LCD_IN_00
Text HLabel 1650 2950 0    50   Input ~ 0
LCD_IN_01
Text HLabel 1650 3050 0    50   Input ~ 0
LCD_IN_02
Text HLabel 1650 3150 0    50   Input ~ 0
LCD_IN_03
Text HLabel 1650 3250 0    50   Input ~ 0
LCD_IN_04
Text HLabel 1650 3350 0    50   Input ~ 0
LCD_IN_05
Text HLabel 1650 3450 0    50   Input ~ 0
LCD_IN_06
Text HLabel 1650 3550 0    50   Input ~ 0
LCD_IN_07
Text HLabel 3450 1650 0    50   Input ~ 0
LCD_IN_08
Text HLabel 3450 1750 0    50   Input ~ 0
LCD_IN_09
Text HLabel 3450 1850 0    50   Input ~ 0
LCD_IN_10
Text HLabel 3450 1950 0    50   Input ~ 0
LCD_IN_11
Text HLabel 3450 2050 0    50   Input ~ 0
LCD_IN_12
Text HLabel 3450 2150 0    50   Input ~ 0
LCD_IN_13
Text HLabel 3450 2250 0    50   Input ~ 0
LCD_IN_14
Text HLabel 3450 2350 0    50   Input ~ 0
LCD_IN_15
Text HLabel 3450 2850 0    50   Input ~ 0
LCD_IN_16
Text HLabel 3450 2950 0    50   Input ~ 0
LCD_IN_17
Text HLabel 3450 3050 0    50   Input ~ 0
LCD_IN_18
Text HLabel 3450 3150 0    50   Input ~ 0
LCD_IN_19
Text HLabel 3450 3250 0    50   Input ~ 0
LCD_IN_20
Text HLabel 3450 3350 0    50   Input ~ 0
LCD_IN_21
NoConn ~ 3450 3450
NoConn ~ 3450 3550
NoConn ~ 1650 2250
NoConn ~ 1650 2350
Text GLabel 1650 2650 0    50   Input ~ 0
GND
Text GLabel 3450 1450 0    50   Input ~ 0
GND
Text GLabel 1650 1450 0    50   Input ~ 0
GND
Text GLabel 3450 2650 0    50   Input ~ 0
GND
Text GLabel 5350 1500 0    50   Input ~ 0
GND
Text GLabel 5350 2700 0    50   Input ~ 0
GND
$Comp
L immobilizer_sym:+3.3V-OLIMEX_Power #PWR0121
U 1 1 5F77CDE3
P 1550 900
F 0 "#PWR0121" H 1550 750 50  0001 C CNN
F 1 "+3.3V" H 1565 1073 50  0000 C CNN
F 2 "" H 1550 900 60  0000 C CNN
F 3 "" H 1550 900 60  0000 C CNN
	1    1550 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 900  1750 900 
Text GLabel 1750 900  2    50   Input ~ 0
VCC33
Text GLabel 5350 1400 0    50   Input ~ 0
VCC33
Text GLabel 5350 2600 0    50   Input ~ 0
VCC33
Text GLabel 3450 2550 0    50   Input ~ 0
GND
Text GLabel 1650 2550 0    50   Input ~ 0
GND
Text GLabel 3450 1350 0    50   Input ~ 0
GND
Text GLabel 1650 1350 0    50   Input ~ 0
GND
Text HLabel 5350 1700 0    50   Input ~ 0
CNT_OUT_00
Text HLabel 5350 1800 0    50   Input ~ 0
CNT_OUT_01
Text HLabel 5350 1900 0    50   Input ~ 0
CNT_OUT_02
Text HLabel 5350 2000 0    50   Input ~ 0
CNT_OUT_03
Text HLabel 5350 2100 0    50   Input ~ 0
CNT_OUT_04
Text HLabel 5350 2200 0    50   Input ~ 0
CNT_OUT_05
Text HLabel 5350 2300 0    50   Input ~ 0
CNT_OUT_06
Text HLabel 5350 2400 0    50   Input ~ 0
CNT_OUT_07
Text HLabel 5350 2900 0    50   Input ~ 0
CNT_OUT_08
Text HLabel 5350 3000 0    50   Input ~ 0
CNT_OUT_09
Text HLabel 5350 3100 0    50   Input ~ 0
CNT_OUT_10
Text HLabel 5350 3200 0    50   Input ~ 0
CNT_OUT_11
Text HLabel 5350 3300 0    50   Input ~ 0
CNT_OUT_12
Text HLabel 5350 3400 0    50   Input ~ 0
CNT_OUT_13
Text HLabel 5350 3500 0    50   Input ~ 0
CNT_OUT_14
Text HLabel 5350 3600 0    50   Input ~ 0
CNT_OUT_15
$Comp
L Connector:DB9_Male J?
U 1 1 5F8F347B
P 9850 1850
AR Path="/5F8F347B" Ref="J?"  Part="1" 
AR Path="/5E8ACECD/5F8F347B" Ref="J5"  Part="1" 
F 0 "J5" H 9600 1150 50  0000 C CNN
F 1 "DB9_Male" H 9600 1250 50  0000 C CNN
F 2 "sparkfun/SparkFun-KiCad-Libraries/Footprints/Connectors.pretty:DB9_MALE" H 9850 1850 50  0001 C CNN
F 3 " ~" H 9850 1850 50  0001 C CNN
	1    9850 1850
	1    0    0    -1  
$EndComp
Text GLabel 9550 2250 0    50   Input ~ 0
G_PAD_IN_1
Text GLabel 9550 2050 0    50   Input ~ 0
G_PAD_IN_2
Text GLabel 9550 1850 0    50   Input ~ 0
G_PAD_IN_3
Text GLabel 9550 1650 0    50   Input ~ 0
G_PAD_IN_4
Text GLabel 9550 2150 0    50   Input ~ 0
G_PAD_IN_6
Text GLabel 9550 1550 0    50   Input ~ 0
G_PAD_IN_9
Wire Wire Line
	9550 1450 9000 1450
Wire Wire Line
	9550 1750 9000 1750
$Comp
L power:GND #PWR0123
U 1 1 5F8FDF99
P 9000 1750
F 0 "#PWR0123" H 9000 1500 50  0001 C CNN
F 1 "GND" H 8850 1700 50  0000 C CNN
F 2 "" H 9000 1750 50  0001 C CNN
F 3 "" H 9000 1750 50  0001 C CNN
	1    9000 1750
	1    0    0    -1  
$EndComp
$Comp
L immobilizer_sym:SN74ALVC164245DL(SSOP48) U6
U 3 1 5F974C19
P 7550 4500
F 0 "U6" H 7750 4850 60  0000 L CNN
F 1 "SN74ALVC164245" H 7250 4950 60  0000 L CNN
F 2 "KiCad/kicad-footprints/Package_SO.pretty:TSSOP-48_6.1x12.5mm_P0.5mm" H 7250 4500 60  0001 C CNN
F 3 "" H 7250 4500 60  0000 C CNN
	3    7550 4500
	1    0    0    -1  
$EndComp
$Comp
L immobilizer_sym:SN74ALVC164245DL(SSOP48) U6
U 4 1 5F974C1F
P 7550 5600
F 0 "U6" H 7750 5250 60  0000 L CNN
F 1 "SN74ALVC164245" H 7200 5150 60  0000 L CNN
F 2 "KiCad/kicad-footprints/Package_SO.pretty:TSSOP-48_6.1x12.5mm_P0.5mm" H 7250 5600 60  0001 C CNN
F 3 "" H 7250 5600 60  0000 C CNN
	4    7550 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 5600 7150 5600
Wire Wire Line
	7150 5600 7150 5700
Wire Wire Line
	7150 5900 7250 5900
Wire Wire Line
	7250 5800 7150 5800
Connection ~ 7150 5800
Wire Wire Line
	7150 5800 7150 5900
Wire Wire Line
	7250 5700 7150 5700
Connection ~ 7150 5700
Wire Wire Line
	7150 5700 7150 5800
$Comp
L immobilizer_sym:GND-OLIMEX_Power #PWR0125
U 1 1 5F974C45
P 6600 5950
F 0 "#PWR0125" H 6600 5700 50  0001 C CNN
F 1 "GND" H 6605 5777 50  0000 C CNN
F 2 "" H 6600 5950 60  0000 C CNN
F 3 "" H 6600 5950 60  0000 C CNN
	1    6600 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 5300 7150 5300
Wire Wire Line
	7250 5400 7150 5400
Wire Wire Line
	7150 5400 7150 5300
Connection ~ 7150 5300
$Comp
L immobilizer_sym:PWR_FLAG-OLIMEX_Power #FLG0107
U 1 1 5F974C50
P 7000 5300
F 0 "#FLG0107" H 7000 5395 50  0001 C CNN
F 1 "PWR_FLAG" H 7000 5523 50  0000 C CNN
F 2 "" H 7000 5300 60  0000 C CNN
F 3 "" H 7000 5300 60  0000 C CNN
	1    7000 5300
	1    0    0    -1  
$EndComp
$Comp
L immobilizer_sym:+3.3V-OLIMEX_Power #PWR0126
U 1 1 5F974C5F
P 6600 4200
F 0 "#PWR0126" H 6600 4050 50  0001 C CNN
F 1 "+3.3V" H 6615 4373 50  0000 C CNN
F 2 "" H 6600 4200 60  0000 C CNN
F 3 "" H 6600 4200 60  0000 C CNN
	1    6600 4200
	1    0    0    -1  
$EndComp
$Comp
L immobilizer_sym:SN74ALVC164245DL(SSOP48) U6
U 1 1 5F974C66
P 7550 1900
F 0 "U6" H 7550 2687 60  0000 C CNN
F 1 "SN74ALVC164245" H 7550 2581 60  0000 C CNN
F 2 "KiCad/kicad-footprints/Package_SO.pretty:TSSOP-48_6.1x12.5mm_P0.5mm" H 7250 1900 60  0001 C CNN
F 3 "" H 7250 1900 60  0000 C CNN
	1    7550 1900
	1    0    0    -1  
$EndComp
$Comp
L immobilizer_sym:SN74ALVC164245DL(SSOP48) U6
U 2 1 5F974C6C
P 7550 3100
F 0 "U6" H 7550 2450 60  0000 C CNN
F 1 "SN74ALVC164245" H 7550 2350 60  0000 C CNN
F 2 "KiCad/kicad-footprints/Package_SO.pretty:TSSOP-48_6.1x12.5mm_P0.5mm" H 7250 3100 60  0001 C CNN
F 3 "" H 7250 3100 60  0000 C CNN
	2    7550 3100
	1    0    0    -1  
$EndComp
NoConn ~ 7850 3500
NoConn ~ 7850 3600
NoConn ~ 7850 2300
NoConn ~ 7850 1700
NoConn ~ 7250 3500
NoConn ~ 7250 3600
NoConn ~ 7250 2300
NoConn ~ 7250 1700
Text GLabel 7250 2700 0    50   Input ~ 0
GND
Text GLabel 7250 1500 0    50   Input ~ 0
GND
Text GLabel 7250 2600 0    50   Input ~ 0
GND
Text GLabel 7850 2900 2    50   Input ~ 0
G_PAD_IN_1
Text GLabel 7850 3000 2    50   Input ~ 0
G_PAD_IN_2
Text GLabel 7850 3100 2    50   Input ~ 0
G_PAD_IN_3
Text GLabel 7850 3200 2    50   Input ~ 0
G_PAD_IN_4
Text GLabel 7850 3300 2    50   Input ~ 0
G_PAD_IN_6
Text GLabel 7850 3400 2    50   Input ~ 0
G_PAD_IN_9
Text HLabel 7250 2900 0    50   Input ~ 0
PAD_IN_1
Text HLabel 7250 3000 0    50   Input ~ 0
PAD_IN_2
Text HLabel 7250 3100 0    50   Input ~ 0
PAD_IN_3
Text HLabel 7250 3200 0    50   Input ~ 0
PAD_IN_4
Text HLabel 7250 3300 0    50   Input ~ 0
PAD_IN_6
Text HLabel 7250 3400 0    50   Input ~ 0
PAD_IN_9
NoConn ~ 7250 2200
NoConn ~ 7250 2100
NoConn ~ 7250 2000
NoConn ~ 7250 1900
NoConn ~ 7250 1800
NoConn ~ 7850 1800
NoConn ~ 7850 1900
NoConn ~ 7850 2000
NoConn ~ 7850 2100
NoConn ~ 7850 2200
Text GLabel 7250 1400 0    50   Input ~ 0
VCC33
Text HLabel 7250 2400 0    50   Input ~ 0
PAD_SEL_5
Text GLabel 7850 2400 2    50   Input ~ 0
G_PAD_SEL_7
Text GLabel 9550 1950 0    50   Input ~ 0
G_PAD_SEL_7
NoConn ~ 1650 2150
NoConn ~ 1650 2050
NoConn ~ 1650 1950
NoConn ~ 1650 1850
NoConn ~ 1650 1750
NoConn ~ 1650 1650
NoConn ~ 2250 1650
NoConn ~ 2250 1750
NoConn ~ 2250 1850
NoConn ~ 2250 1950
NoConn ~ 2250 2050
NoConn ~ 2250 2150
Wire Wire Line
	7150 5900 6900 5900
Connection ~ 7150 5900
Wire Wire Line
	6600 5900 6600 5950
$Comp
L Device:C C16
U 1 1 5E8F0207
P 6600 5600
F 0 "C16" H 6600 5700 50  0000 L CNN
F 1 "100nF" H 6600 5500 50  0000 L CNN
F 2 "KiCad/kicad-footprints/Capacitor_SMD.pretty:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6638 5450 50  0001 C CNN
F 3 "~" H 6600 5600 50  0001 C CNN
	1    6600 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C18
U 1 1 5E8F0AE7
P 6900 5600
F 0 "C18" H 6900 5700 50  0000 L CNN
F 1 "100nF" H 6900 5500 50  0000 L CNN
F 2 "KiCad/kicad-footprints/Capacitor_SMD.pretty:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6938 5450 50  0001 C CNN
F 3 "~" H 6900 5600 50  0001 C CNN
	1    6900 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 5450 6900 5400
Wire Wire Line
	6900 5400 7150 5400
Connection ~ 7150 5400
Wire Wire Line
	6900 5750 6900 5900
Connection ~ 6900 5900
Wire Wire Line
	6900 5900 6600 5900
Wire Wire Line
	6600 5300 6600 5450
Wire Wire Line
	6600 5750 6600 5900
Connection ~ 6600 5900
Wire Wire Line
	7250 4500 7150 4500
Wire Wire Line
	7150 4500 7150 4600
Wire Wire Line
	7150 4800 7250 4800
Wire Wire Line
	7250 4700 7150 4700
Connection ~ 7150 4700
Wire Wire Line
	7150 4700 7150 4800
Wire Wire Line
	7250 4600 7150 4600
Connection ~ 7150 4600
Wire Wire Line
	7150 4600 7150 4700
$Comp
L immobilizer_sym:GND-OLIMEX_Power #PWR0112
U 1 1 5E90AEA9
P 6600 4850
F 0 "#PWR0112" H 6600 4600 50  0001 C CNN
F 1 "GND" H 6605 4677 50  0000 C CNN
F 2 "" H 6600 4850 60  0000 C CNN
F 3 "" H 6600 4850 60  0000 C CNN
	1    6600 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7250 4200 7150 4200
Wire Wire Line
	7250 4300 7150 4300
Wire Wire Line
	7150 4300 7150 4200
Connection ~ 7150 4200
$Comp
L immobilizer_sym:PWR_FLAG-OLIMEX_Power #FLG0101
U 1 1 5E90AEB3
P 7000 4200
F 0 "#FLG0101" H 7000 4295 50  0001 C CNN
F 1 "PWR_FLAG" H 7000 4423 50  0000 C CNN
F 2 "" H 7000 4200 60  0000 C CNN
F 3 "" H 7000 4200 60  0000 C CNN
	1    7000 4200
	1    0    0    -1  
$EndComp
Connection ~ 7000 4200
Wire Wire Line
	7000 4200 7150 4200
Wire Wire Line
	6600 4200 7000 4200
Wire Wire Line
	7150 4800 6900 4800
Connection ~ 7150 4800
Wire Wire Line
	6600 4800 6600 4850
$Comp
L Device:C C15
U 1 1 5E90AEC6
P 6600 4500
F 0 "C15" H 6600 4600 50  0000 L CNN
F 1 "100nF" H 6600 4400 50  0000 L CNN
F 2 "KiCad/kicad-footprints/Capacitor_SMD.pretty:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6638 4350 50  0001 C CNN
F 3 "~" H 6600 4500 50  0001 C CNN
	1    6600 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C17
U 1 1 5E90AECC
P 6900 4500
F 0 "C17" H 6900 4600 50  0000 L CNN
F 1 "100nF" H 6900 4400 50  0000 L CNN
F 2 "KiCad/kicad-footprints/Capacitor_SMD.pretty:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6938 4350 50  0001 C CNN
F 3 "~" H 6900 4500 50  0001 C CNN
	1    6900 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6900 4350 6900 4300
Wire Wire Line
	6900 4300 7150 4300
Connection ~ 7150 4300
Wire Wire Line
	6900 4650 6900 4800
Connection ~ 6900 4800
Wire Wire Line
	6900 4800 6600 4800
Wire Wire Line
	6600 4200 6600 4350
Wire Wire Line
	6600 4650 6600 4800
Connection ~ 6600 4800
Connection ~ 6600 4200
Connection ~ 7000 5300
Wire Wire Line
	7000 5300 7150 5300
Wire Wire Line
	6600 5300 7000 5300
$Comp
L immobilizer_sym:SN74ALVC164245DL(SSOP48) U5
U 3 1 5F701DF3
P 5650 4500
F 0 "U5" H 5850 4850 60  0000 L CNN
F 1 "SN74ALVC164245" H 5350 4950 60  0000 L CNN
F 2 "KiCad/kicad-footprints/Package_SO.pretty:TSSOP-48_6.1x12.5mm_P0.5mm" H 5350 4500 60  0001 C CNN
F 3 "" H 5350 4500 60  0000 C CNN
	3    5650 4500
	1    0    0    -1  
$EndComp
Text GLabel 4700 5300 0    50   Input ~ 0
CONS_VCC
$Comp
L immobilizer_sym:+3.3V-OLIMEX_Power #PWR0113
U 1 1 5E993C08
P 4700 4200
F 0 "#PWR0113" H 4700 4050 50  0001 C CNN
F 1 "+3.3V" H 4715 4373 50  0000 C CNN
F 2 "" H 4700 4200 60  0000 C CNN
F 3 "" H 4700 4200 60  0000 C CNN
	1    4700 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 4500 5250 4500
Wire Wire Line
	5250 4500 5250 4600
Wire Wire Line
	5250 4800 5350 4800
Wire Wire Line
	5350 4700 5250 4700
Connection ~ 5250 4700
Wire Wire Line
	5250 4700 5250 4800
Wire Wire Line
	5350 4600 5250 4600
Connection ~ 5250 4600
Wire Wire Line
	5250 4600 5250 4700
$Comp
L immobilizer_sym:GND-OLIMEX_Power #PWR0114
U 1 1 5E993C17
P 4700 4850
F 0 "#PWR0114" H 4700 4600 50  0001 C CNN
F 1 "GND" H 4705 4677 50  0000 C CNN
F 2 "" H 4700 4850 60  0000 C CNN
F 3 "" H 4700 4850 60  0000 C CNN
	1    4700 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 4200 5250 4200
Wire Wire Line
	5350 4300 5250 4300
Wire Wire Line
	5250 4300 5250 4200
Connection ~ 5250 4200
$Comp
L immobilizer_sym:PWR_FLAG-OLIMEX_Power #FLG0102
U 1 1 5E993C21
P 5100 4200
F 0 "#FLG0102" H 5100 4295 50  0001 C CNN
F 1 "PWR_FLAG" H 5100 4423 50  0000 C CNN
F 2 "" H 5100 4200 60  0000 C CNN
F 3 "" H 5100 4200 60  0000 C CNN
	1    5100 4200
	1    0    0    -1  
$EndComp
Connection ~ 5100 4200
Wire Wire Line
	5100 4200 5250 4200
Wire Wire Line
	4700 4200 5100 4200
Wire Wire Line
	5250 4800 5000 4800
Connection ~ 5250 4800
Wire Wire Line
	4700 4800 4700 4850
$Comp
L Device:C C11
U 1 1 5E993C2D
P 4700 4500
F 0 "C11" H 4700 4600 50  0000 L CNN
F 1 "100nF" H 4700 4400 50  0000 L CNN
F 2 "KiCad/kicad-footprints/Capacitor_SMD.pretty:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4738 4350 50  0001 C CNN
F 3 "~" H 4700 4500 50  0001 C CNN
	1    4700 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C13
U 1 1 5E993C33
P 5000 4500
F 0 "C13" H 5000 4600 50  0000 L CNN
F 1 "100nF" H 5000 4400 50  0000 L CNN
F 2 "KiCad/kicad-footprints/Capacitor_SMD.pretty:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5038 4350 50  0001 C CNN
F 3 "~" H 5000 4500 50  0001 C CNN
	1    5000 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 4350 5000 4300
Wire Wire Line
	5000 4300 5250 4300
Connection ~ 5250 4300
Wire Wire Line
	5000 4650 5000 4800
Connection ~ 5000 4800
Wire Wire Line
	5000 4800 4700 4800
Wire Wire Line
	4700 4200 4700 4350
Wire Wire Line
	4700 4650 4700 4800
Connection ~ 4700 4800
Connection ~ 4700 4200
Wire Wire Line
	5350 5600 5250 5600
Wire Wire Line
	5250 5600 5250 5700
Wire Wire Line
	5250 5900 5350 5900
Wire Wire Line
	5350 5800 5250 5800
Connection ~ 5250 5800
Wire Wire Line
	5250 5800 5250 5900
Wire Wire Line
	5350 5700 5250 5700
Connection ~ 5250 5700
Wire Wire Line
	5250 5700 5250 5800
$Comp
L immobilizer_sym:GND-OLIMEX_Power #PWR0115
U 1 1 5E993C4C
P 4700 5950
F 0 "#PWR0115" H 4700 5700 50  0001 C CNN
F 1 "GND" H 4705 5777 50  0000 C CNN
F 2 "" H 4700 5950 60  0000 C CNN
F 3 "" H 4700 5950 60  0000 C CNN
	1    4700 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5350 5300 5250 5300
Wire Wire Line
	5350 5400 5250 5400
Wire Wire Line
	5250 5400 5250 5300
Connection ~ 5250 5300
$Comp
L immobilizer_sym:PWR_FLAG-OLIMEX_Power #FLG0103
U 1 1 5E993C56
P 5100 5300
F 0 "#FLG0103" H 5100 5395 50  0001 C CNN
F 1 "PWR_FLAG" H 5100 5523 50  0000 C CNN
F 2 "" H 5100 5300 60  0000 C CNN
F 3 "" H 5100 5300 60  0000 C CNN
	1    5100 5300
	1    0    0    -1  
$EndComp
Connection ~ 5100 5300
Wire Wire Line
	5100 5300 5250 5300
Wire Wire Line
	4700 5300 5100 5300
Wire Wire Line
	5250 5900 5000 5900
Connection ~ 5250 5900
Wire Wire Line
	4700 5900 4700 5950
$Comp
L Device:C C12
U 1 1 5E993C62
P 4700 5600
F 0 "C12" H 4700 5700 50  0000 L CNN
F 1 "100nF" H 4700 5500 50  0000 L CNN
F 2 "KiCad/kicad-footprints/Capacitor_SMD.pretty:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4738 5450 50  0001 C CNN
F 3 "~" H 4700 5600 50  0001 C CNN
	1    4700 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C14
U 1 1 5E993C68
P 5000 5600
F 0 "C14" H 5000 5700 50  0000 L CNN
F 1 "100nF" H 5000 5500 50  0000 L CNN
F 2 "KiCad/kicad-footprints/Capacitor_SMD.pretty:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 5038 5450 50  0001 C CNN
F 3 "~" H 5000 5600 50  0001 C CNN
	1    5000 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 5450 5000 5400
Wire Wire Line
	5000 5400 5250 5400
Connection ~ 5250 5400
Wire Wire Line
	5000 5750 5000 5900
Connection ~ 5000 5900
Wire Wire Line
	5000 5900 4700 5900
Wire Wire Line
	4700 5300 4700 5450
Wire Wire Line
	4700 5750 4700 5900
Connection ~ 4700 5900
Text GLabel 2800 5300 0    50   Input ~ 0
CONS_VCC
$Comp
L immobilizer_sym:+3.3V-OLIMEX_Power #PWR0116
U 1 1 5E9B0204
P 2800 4200
F 0 "#PWR0116" H 2800 4050 50  0001 C CNN
F 1 "+3.3V" H 2815 4373 50  0000 C CNN
F 2 "" H 2800 4200 60  0000 C CNN
F 3 "" H 2800 4200 60  0000 C CNN
	1    2800 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 4500 3350 4500
Wire Wire Line
	3350 4500 3350 4600
Wire Wire Line
	3350 4800 3450 4800
Wire Wire Line
	3450 4700 3350 4700
Connection ~ 3350 4700
Wire Wire Line
	3350 4700 3350 4800
Wire Wire Line
	3450 4600 3350 4600
Connection ~ 3350 4600
Wire Wire Line
	3350 4600 3350 4700
$Comp
L immobilizer_sym:GND-OLIMEX_Power #PWR0117
U 1 1 5E9B0213
P 2800 4850
F 0 "#PWR0117" H 2800 4600 50  0001 C CNN
F 1 "GND" H 2805 4677 50  0000 C CNN
F 2 "" H 2800 4850 60  0000 C CNN
F 3 "" H 2800 4850 60  0000 C CNN
	1    2800 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 4200 3350 4200
Wire Wire Line
	3450 4300 3350 4300
Wire Wire Line
	3350 4300 3350 4200
Connection ~ 3350 4200
$Comp
L immobilizer_sym:PWR_FLAG-OLIMEX_Power #FLG0104
U 1 1 5E9B021D
P 3200 4200
F 0 "#FLG0104" H 3200 4295 50  0001 C CNN
F 1 "PWR_FLAG" H 3200 4423 50  0000 C CNN
F 2 "" H 3200 4200 60  0000 C CNN
F 3 "" H 3200 4200 60  0000 C CNN
	1    3200 4200
	1    0    0    -1  
$EndComp
Connection ~ 3200 4200
Wire Wire Line
	3200 4200 3350 4200
Wire Wire Line
	2800 4200 3200 4200
Wire Wire Line
	3350 4800 3100 4800
Connection ~ 3350 4800
Wire Wire Line
	2800 4800 2800 4850
$Comp
L Device:C C7
U 1 1 5E9B0229
P 2800 4500
F 0 "C7" H 2800 4600 50  0000 L CNN
F 1 "100nF" H 2800 4400 50  0000 L CNN
F 2 "KiCad/kicad-footprints/Capacitor_SMD.pretty:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2838 4350 50  0001 C CNN
F 3 "~" H 2800 4500 50  0001 C CNN
	1    2800 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C9
U 1 1 5E9B022F
P 3100 4500
F 0 "C9" H 3100 4600 50  0000 L CNN
F 1 "100nF" H 3100 4400 50  0000 L CNN
F 2 "KiCad/kicad-footprints/Capacitor_SMD.pretty:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3138 4350 50  0001 C CNN
F 3 "~" H 3100 4500 50  0001 C CNN
	1    3100 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 4350 3100 4300
Wire Wire Line
	3100 4300 3350 4300
Connection ~ 3350 4300
Wire Wire Line
	3100 4650 3100 4800
Connection ~ 3100 4800
Wire Wire Line
	3100 4800 2800 4800
Wire Wire Line
	2800 4200 2800 4350
Wire Wire Line
	2800 4650 2800 4800
Connection ~ 2800 4800
Connection ~ 2800 4200
Wire Wire Line
	3450 5600 3350 5600
Wire Wire Line
	3350 5600 3350 5700
Wire Wire Line
	3350 5900 3450 5900
Wire Wire Line
	3450 5800 3350 5800
Connection ~ 3350 5800
Wire Wire Line
	3350 5800 3350 5900
Wire Wire Line
	3450 5700 3350 5700
Connection ~ 3350 5700
Wire Wire Line
	3350 5700 3350 5800
$Comp
L immobilizer_sym:GND-OLIMEX_Power #PWR0118
U 1 1 5E9B0248
P 2800 5950
F 0 "#PWR0118" H 2800 5700 50  0001 C CNN
F 1 "GND" H 2805 5777 50  0000 C CNN
F 2 "" H 2800 5950 60  0000 C CNN
F 3 "" H 2800 5950 60  0000 C CNN
	1    2800 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 5300 3350 5300
Wire Wire Line
	3450 5400 3350 5400
Wire Wire Line
	3350 5400 3350 5300
Connection ~ 3350 5300
$Comp
L immobilizer_sym:PWR_FLAG-OLIMEX_Power #FLG0105
U 1 1 5E9B0252
P 3200 5300
F 0 "#FLG0105" H 3200 5395 50  0001 C CNN
F 1 "PWR_FLAG" H 3200 5523 50  0000 C CNN
F 2 "" H 3200 5300 60  0000 C CNN
F 3 "" H 3200 5300 60  0000 C CNN
	1    3200 5300
	1    0    0    -1  
$EndComp
Connection ~ 3200 5300
Wire Wire Line
	3200 5300 3350 5300
Wire Wire Line
	2800 5300 3200 5300
Wire Wire Line
	3350 5900 3100 5900
Connection ~ 3350 5900
Wire Wire Line
	2800 5900 2800 5950
$Comp
L Device:C C8
U 1 1 5E9B025E
P 2800 5600
F 0 "C8" H 2800 5700 50  0000 L CNN
F 1 "100nF" H 2800 5500 50  0000 L CNN
F 2 "KiCad/kicad-footprints/Capacitor_SMD.pretty:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 2838 5450 50  0001 C CNN
F 3 "~" H 2800 5600 50  0001 C CNN
	1    2800 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C10
U 1 1 5E9B0264
P 3100 5600
F 0 "C10" H 3100 5700 50  0000 L CNN
F 1 "100nF" H 3100 5500 50  0000 L CNN
F 2 "KiCad/kicad-footprints/Capacitor_SMD.pretty:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3138 5450 50  0001 C CNN
F 3 "~" H 3100 5600 50  0001 C CNN
	1    3100 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 5450 3100 5400
Wire Wire Line
	3100 5400 3350 5400
Connection ~ 3350 5400
Wire Wire Line
	3100 5750 3100 5900
Connection ~ 3100 5900
Wire Wire Line
	3100 5900 2800 5900
Wire Wire Line
	2800 5300 2800 5450
Wire Wire Line
	2800 5750 2800 5900
Connection ~ 2800 5900
Text GLabel 1000 5300 0    50   Input ~ 0
CONS_VCC
$Comp
L immobilizer_sym:+3.3V-OLIMEX_Power #PWR0119
U 1 1 5E9BA0EB
P 1000 4200
F 0 "#PWR0119" H 1000 4050 50  0001 C CNN
F 1 "+3.3V" H 1015 4373 50  0000 C CNN
F 2 "" H 1000 4200 60  0000 C CNN
F 3 "" H 1000 4200 60  0000 C CNN
	1    1000 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 4500 1550 4500
Wire Wire Line
	1550 4500 1550 4600
Wire Wire Line
	1550 4800 1650 4800
Wire Wire Line
	1650 4700 1550 4700
Connection ~ 1550 4700
Wire Wire Line
	1550 4700 1550 4800
Wire Wire Line
	1650 4600 1550 4600
Connection ~ 1550 4600
Wire Wire Line
	1550 4600 1550 4700
$Comp
L immobilizer_sym:GND-OLIMEX_Power #PWR0120
U 1 1 5E9BA0FA
P 1000 4850
F 0 "#PWR0120" H 1000 4600 50  0001 C CNN
F 1 "GND" H 1005 4677 50  0000 C CNN
F 2 "" H 1000 4850 60  0000 C CNN
F 3 "" H 1000 4850 60  0000 C CNN
	1    1000 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 4200 1550 4200
Wire Wire Line
	1650 4300 1550 4300
Wire Wire Line
	1550 4300 1550 4200
Connection ~ 1550 4200
$Comp
L immobilizer_sym:PWR_FLAG-OLIMEX_Power #FLG0106
U 1 1 5E9BA104
P 1400 4200
F 0 "#FLG0106" H 1400 4295 50  0001 C CNN
F 1 "PWR_FLAG" H 1400 4423 50  0000 C CNN
F 2 "" H 1400 4200 60  0000 C CNN
F 3 "" H 1400 4200 60  0000 C CNN
	1    1400 4200
	1    0    0    -1  
$EndComp
Connection ~ 1400 4200
Wire Wire Line
	1400 4200 1550 4200
Wire Wire Line
	1000 4200 1400 4200
Wire Wire Line
	1550 4800 1300 4800
Connection ~ 1550 4800
Wire Wire Line
	1000 4800 1000 4850
$Comp
L Device:C C3
U 1 1 5E9BA110
P 1000 4500
F 0 "C3" H 1000 4600 50  0000 L CNN
F 1 "100nF" H 1000 4400 50  0000 L CNN
F 2 "KiCad/kicad-footprints/Capacitor_SMD.pretty:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1038 4350 50  0001 C CNN
F 3 "~" H 1000 4500 50  0001 C CNN
	1    1000 4500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 5E9BA116
P 1300 4500
F 0 "C5" H 1300 4600 50  0000 L CNN
F 1 "100nF" H 1300 4400 50  0000 L CNN
F 2 "KiCad/kicad-footprints/Capacitor_SMD.pretty:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1338 4350 50  0001 C CNN
F 3 "~" H 1300 4500 50  0001 C CNN
	1    1300 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 4350 1300 4300
Wire Wire Line
	1300 4300 1550 4300
Connection ~ 1550 4300
Wire Wire Line
	1300 4650 1300 4800
Connection ~ 1300 4800
Wire Wire Line
	1300 4800 1000 4800
Wire Wire Line
	1000 4200 1000 4350
Wire Wire Line
	1000 4650 1000 4800
Connection ~ 1000 4800
Connection ~ 1000 4200
Wire Wire Line
	1650 5600 1550 5600
Wire Wire Line
	1550 5600 1550 5700
Wire Wire Line
	1550 5900 1650 5900
Wire Wire Line
	1650 5800 1550 5800
Connection ~ 1550 5800
Wire Wire Line
	1550 5800 1550 5900
Wire Wire Line
	1650 5700 1550 5700
Connection ~ 1550 5700
Wire Wire Line
	1550 5700 1550 5800
$Comp
L immobilizer_sym:GND-OLIMEX_Power #PWR0124
U 1 1 5E9BA12F
P 1000 5950
F 0 "#PWR0124" H 1000 5700 50  0001 C CNN
F 1 "GND" H 1005 5777 50  0000 C CNN
F 2 "" H 1000 5950 60  0000 C CNN
F 3 "" H 1000 5950 60  0000 C CNN
	1    1000 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	1650 5300 1550 5300
Wire Wire Line
	1650 5400 1550 5400
Wire Wire Line
	1550 5400 1550 5300
Connection ~ 1550 5300
$Comp
L immobilizer_sym:PWR_FLAG-OLIMEX_Power #FLG0108
U 1 1 5E9BA139
P 1400 5300
F 0 "#FLG0108" H 1400 5395 50  0001 C CNN
F 1 "PWR_FLAG" H 1400 5523 50  0000 C CNN
F 2 "" H 1400 5300 60  0000 C CNN
F 3 "" H 1400 5300 60  0000 C CNN
	1    1400 5300
	1    0    0    -1  
$EndComp
Connection ~ 1400 5300
Wire Wire Line
	1400 5300 1550 5300
Wire Wire Line
	1000 5300 1400 5300
Wire Wire Line
	1550 5900 1300 5900
Connection ~ 1550 5900
Wire Wire Line
	1000 5900 1000 5950
$Comp
L Device:C C4
U 1 1 5E9BA145
P 1000 5600
F 0 "C4" H 1000 5700 50  0000 L CNN
F 1 "100nF" H 1000 5500 50  0000 L CNN
F 2 "KiCad/kicad-footprints/Capacitor_SMD.pretty:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1038 5450 50  0001 C CNN
F 3 "~" H 1000 5600 50  0001 C CNN
	1    1000 5600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 5E9BA14B
P 1300 5600
F 0 "C6" H 1300 5700 50  0000 L CNN
F 1 "100nF" H 1300 5500 50  0000 L CNN
F 2 "KiCad/kicad-footprints/Capacitor_SMD.pretty:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1338 5450 50  0001 C CNN
F 3 "~" H 1300 5600 50  0001 C CNN
	1    1300 5600
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 5450 1300 5400
Wire Wire Line
	1300 5400 1550 5400
Connection ~ 1550 5400
Wire Wire Line
	1300 5750 1300 5900
Connection ~ 1300 5900
Wire Wire Line
	1300 5900 1000 5900
Wire Wire Line
	1000 5300 1000 5450
Wire Wire Line
	1000 5750 1000 5900
Connection ~ 1000 5900
$Comp
L power:+5V #PWR0127
U 1 1 5FF080D2
P 9000 1450
F 0 "#PWR0127" H 9000 1300 50  0001 C CNN
F 1 "+5V" H 8850 1500 50  0000 C CNN
F 2 "" H 9000 1450 50  0001 C CNN
F 3 "" H 9000 1450 50  0001 C CNN
	1    9000 1450
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR0128
U 1 1 5FF098F8
P 6600 5300
F 0 "#PWR0128" H 6600 5150 50  0001 C CNN
F 1 "+5V" H 6615 5473 50  0000 C CNN
F 2 "" H 6600 5300 50  0001 C CNN
F 3 "" H 6600 5300 50  0001 C CNN
	1    6600 5300
	1    0    0    -1  
$EndComp
Connection ~ 6600 5300
$Comp
L immobilizer_sym:PWR_FLAG-OLIMEX_Power #FLG0109
U 1 1 5F4A60FA
P 8600 5850
F 0 "#FLG0109" H 8600 5945 50  0001 C CNN
F 1 "PWR_FLAG" H 8600 6073 50  0000 C CNN
F 2 "" H 8600 5850 60  0000 C CNN
F 3 "" H 8600 5850 60  0000 C CNN
	1    8600 5850
	1    0    0    -1  
$EndComp
$EndSCHEMATC
