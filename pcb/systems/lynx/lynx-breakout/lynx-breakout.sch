EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x26_Female J1
U 1 1 5F5C2B99
P 3700 5450
F 0 "J1" H 3592 3925 50  0000 C CNN
F 1 "Conn_01x26_Female" H 3592 4016 50  0000 C CNN
F 2 "lynx-breakout:video_overlay_25" H 3700 5450 50  0001 C CNN
F 3 "~" H 3700 5450 50  0001 C CNN
	1    3700 5450
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x14_Female J2
U 1 1 5F5DB20A
P 3700 3100
F 0 "J2" H 3592 2175 50  0000 C CNN
F 1 "Conn_01x14_Female" H 3592 2266 50  0000 C CNN
F 2 "lynx-breakout:controls_overlay_25" H 3700 3100 50  0001 C CNN
F 3 "~" H 3700 3100 50  0001 C CNN
	1    3700 3100
	-1   0    0    -1  
$EndComp
Text GLabel 3900 3700 2    50   Input ~ 0
BTN_A
Text GLabel 3900 3600 2    50   Input ~ 0
BTN_B
Text GLabel 3900 3500 2    50   Input ~ 0
BTN_O2
Text GLabel 3900 3400 2    50   Input ~ 0
BTN_PAUSE
Text GLabel 3900 3300 2    50   Input ~ 0
BTN_O1
Text GLabel 3900 3100 2    50   Input ~ 0
BTN_PWR
Text GLabel 3900 2900 2    50   Input ~ 0
BTN_RIGHT
Text GLabel 3900 2800 2    50   Input ~ 0
BTN_DOWN
Text GLabel 3900 2700 2    50   Input ~ 0
BTN_UP
Text GLabel 3900 2600 2    50   Input ~ 0
BTN_LEFT
Text GLabel 3900 6750 2    50   Input ~ 0
GND
Text GLabel 3900 6250 2    50   Input ~ 0
VBLn
Text GLabel 3900 6150 2    50   Input ~ 0
PXL_D2
Text GLabel 3900 6050 2    50   Input ~ 0
PXL_D0
Text GLabel 3900 5950 2    50   Input ~ 0
PXL_D3
Text GLabel 3900 5850 2    50   Input ~ 0
PXL_D1
Text GLabel 3900 5650 2    50   Input ~ 0
CLK_C
Text GLabel 3900 5450 2    50   Input ~ 0
CLK_B
Text GLabel 3900 5250 2    50   Input ~ 0
CLK_A
Text GLabel 3900 5050 2    50   Input ~ 0
HBL
Text GLabel 3900 4250 2    50   Input ~ 0
VCC
$Comp
L Connector_Generic:Conn_01x24 J3
U 1 1 5F5DFB19
P 5950 4600
F 0 "J3" H 6030 4592 50  0000 L CNN
F 1 "Conn_01x24" H 6030 4501 50  0000 L CNN
F 2 "Connector_FFC-FPC:Hirose_FH12-24S-0.5SH_1x24-1MP_P0.50mm_Horizontal" H 5950 4600 50  0001 C CNN
F 3 "~" H 5950 4600 50  0001 C CNN
	1    5950 4600
	1    0    0    1   
$EndComp
Text GLabel 5750 4800 0    50   Input ~ 0
PXL_D2
Text GLabel 5750 4900 0    50   Input ~ 0
PXL_D3
Text GLabel 5750 5000 0    50   Input ~ 0
VCC
Text GLabel 5750 5100 0    50   Input ~ 0
HBL
Text GLabel 5750 5200 0    50   Input ~ 0
CLK_A
Text GLabel 5750 5300 0    50   Input ~ 0
CLK_B
Text GLabel 5750 5400 0    50   Input ~ 0
CLK_C
Text GLabel 5750 5500 0    50   Input ~ 0
PXL_D1
Text GLabel 5750 5600 0    50   Input ~ 0
PXL_D0
Text GLabel 5750 5700 0    50   Input ~ 0
VBLn
Text GLabel 5750 4600 0    50   Input ~ 0
GND
Text GLabel 5750 3900 0    50   Input ~ 0
BTN_A
Text GLabel 5750 4000 0    50   Input ~ 0
BTN_B
Text GLabel 5750 3800 0    50   Input ~ 0
BTN_O2
Text GLabel 5750 4100 0    50   Input ~ 0
BTN_PAUSE
Text GLabel 5750 3700 0    50   Input ~ 0
BTN_O1
Text GLabel 5750 3600 0    50   Input ~ 0
BTN_PWR
Text GLabel 5750 3500 0    50   Input ~ 0
BTN_RIGHT
Text GLabel 5750 4200 0    50   Input ~ 0
BTN_DOWN
Text GLabel 5750 3400 0    50   Input ~ 0
BTN_UP
Text GLabel 5750 4300 0    50   Input ~ 0
BTN_LEFT
$EndSCHEMATC
