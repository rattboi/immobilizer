EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x40_Male J1
U 1 1 5F6054C2
P 3450 3150
F 0 "J1" H 3422 3032 50  0000 R CNN
F 1 "Immobilizer Interface" H 3422 3123 50  0000 R CNN
F 2 "immobilizer-interface:PinHeader_2x20_P2.54mm_Vertical" H 3450 3150 50  0001 C CNN
F 3 "~" H 3450 3150 50  0001 C CNN
	1    3450 3150
	1    0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x24 J2
U 1 1 5F60D852
P 5200 3250
F 0 "J2" H 5118 1825 50  0000 C CNN
F 1 "Lynx" H 5118 1916 50  0000 C CNN
F 2 "immobilizer-interface:Hirose_FH12-24S-0.5SH_1x24-1MP_P0.50mm_Horizontal-flipped" H 5200 3250 50  0001 C CNN
F 3 "~" H 5200 3250 50  0001 C CNN
	1    5200 3250
	1    0    0    -1  
$EndComp
Text GLabel 5000 2150 0    50   Input ~ 0
VBLn
Text GLabel 5000 2250 0    50   Input ~ 0
PXL_D0
Text GLabel 5000 2350 0    50   Input ~ 0
PXL_d1
Text GLabel 5000 2450 0    50   Input ~ 0
CLK_C
Text GLabel 5000 2550 0    50   Input ~ 0
CLK_B
Text GLabel 5000 2650 0    50   Input ~ 0
CLK_A
Text GLabel 5000 2750 0    50   Input ~ 0
HBL
Text GLabel 5000 2850 0    50   Input ~ 0
VCC
Text GLabel 5000 2950 0    50   Input ~ 0
PXL_D3
Text GLabel 5000 3050 0    50   Input ~ 0
PXL_D2
Text GLabel 5000 3250 0    50   Input ~ 0
GND
Text GLabel 5000 3550 0    50   Input ~ 0
BTN_LEFT
Text GLabel 5000 3650 0    50   Input ~ 0
BTN_DOWN
Text GLabel 5000 3750 0    50   Input ~ 0
BTN_PAUSE
Text GLabel 5000 3850 0    50   Input ~ 0
BTN_B
Text GLabel 5000 3950 0    50   Input ~ 0
BTN_A
Text GLabel 5000 4050 0    50   Input ~ 0
BTN_OPT2
Text GLabel 5000 4150 0    50   Input ~ 0
BTN_OPT1
Text GLabel 5000 4250 0    50   Input ~ 0
BTN_PWR
Text GLabel 5000 4350 0    50   Input ~ 0
BTN_RIGHT
Text GLabel 5000 4450 0    50   Input ~ 0
BTN_UP
Text GLabel 3650 5050 2    50   Input ~ 0
GND
Text GLabel 3650 4950 2    50   Input ~ 0
VCC
Text GLabel 3650 1150 2    50   Input ~ 0
VBLn
Text GLabel 3650 1250 2    50   Input ~ 0
PXL_D0
Text GLabel 3650 1350 2    50   Input ~ 0
PXL_d1
Text GLabel 3650 1450 2    50   Input ~ 0
CLK_C
Text GLabel 3650 1550 2    50   Input ~ 0
CLK_B
Text GLabel 3650 1650 2    50   Input ~ 0
CLK_A
Text GLabel 3650 1750 2    50   Input ~ 0
HBL
Text GLabel 3650 1850 2    50   Input ~ 0
PXL_D3
Text GLabel 3650 1950 2    50   Input ~ 0
PXL_D2
Text GLabel 3650 4650 2    50   Input ~ 0
BTN_UP
Text GLabel 3650 4550 2    50   Input ~ 0
BTN_RIGHT
Text GLabel 3650 4450 2    50   Input ~ 0
BTN_PWR
Text GLabel 3650 4350 2    50   Input ~ 0
BTN_OPT1
Text GLabel 3650 4250 2    50   Input ~ 0
BTN_OPT2
Text GLabel 3650 3750 2    50   Input ~ 0
BTN_LEFT
Text GLabel 3650 3850 2    50   Input ~ 0
BTN_DOWN
Text GLabel 3650 3950 2    50   Input ~ 0
BTN_PAUSE
Text GLabel 3650 4050 2    50   Input ~ 0
BTN_B
Text GLabel 3650 4150 2    50   Input ~ 0
BTN_A
$EndSCHEMATC
