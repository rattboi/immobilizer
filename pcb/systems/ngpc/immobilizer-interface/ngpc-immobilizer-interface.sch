EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x20_Female J1
U 1 1 5F4F3391
P 2100 5950
F 0 "J1" V 2200 5800 50  0000 L CNN
F 1 "NGPC Controls" V 2200 6050 50  0000 L CNN
F 2 "immobilizer-interface:Hirose_FH12-20S-0.5SH_1x20-1MP_P0.50mm_Horizontal" H 2100 5950 50  0001 C CNN
F 3 "~" H 2100 5950 50  0001 C CNN
	1    2100 5950
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x40_Male J2
U 1 1 5F4F968C
P 4000 4300
F 0 "J2" H 4050 5417 50  0000 C CNN
F 1 "Immobilizer Interface" H 3950 6450 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x20_P2.54mm_Vertical" H 4000 4300 50  0001 C CNN
F 3 "~" H 4000 4300 50  0001 C CNN
	1    4000 4300
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x36_Female J3
U 1 1 5F4F4994
P 2100 2700
F 0 "J3" V 2250 2500 50  0000 L CNN
F 1 "NGPC Video" V 2250 2750 50  0000 L CNN
F 2 "Connector_FFC-FPC:Hirose_FH12-36S-0.5SH_1x36-1MP_P0.50mm_Horizontal" H 2100 2700 50  0001 C CNN
F 3 "~" H 2100 2700 50  0001 C CNN
	1    2100 2700
	-1   0    0    1   
$EndComp
Text GLabel 2300 900  2    50   Input ~ 0
GND
Text GLabel 3800 6200 0    50   Input ~ 0
GND
Text GLabel 2300 2100 2    50   Input ~ 0
CONS_PWR
Text GLabel 3800 6100 0    50   Input ~ 0
CONS_PWR
Text GLabel 2300 4100 2    50   Input ~ 0
VSYNC
Text GLabel 2300 3400 2    50   Input ~ 0
HSYNC
Text GLabel 2300 1700 2    50   Input ~ 0
DOTCLK
Text GLabel 2300 3300 2    50   Input ~ 0
R0
Text GLabel 2300 3000 2    50   Input ~ 0
R3
Text GLabel 2300 2900 2    50   Input ~ 0
G0
Text GLabel 2300 2800 2    50   Input ~ 0
G1
Text GLabel 2300 2700 2    50   Input ~ 0
G2
Text GLabel 2300 2600 2    50   Input ~ 0
G3
Text GLabel 2300 2500 2    50   Input ~ 0
B0
Text GLabel 2300 2400 2    50   Input ~ 0
B1
Text GLabel 2300 2300 2    50   Input ~ 0
B2
Text GLabel 2300 2200 2    50   Input ~ 0
B3
Text GLabel 3800 2300 0    50   Input ~ 0
DOTCLK
Text GLabel 3800 2400 0    50   Input ~ 0
B3
Text GLabel 3800 2500 0    50   Input ~ 0
B2
Text GLabel 3800 2600 0    50   Input ~ 0
B1
Text GLabel 3800 2700 0    50   Input ~ 0
B0
Text GLabel 3800 2800 0    50   Input ~ 0
G3
Text GLabel 3800 2900 0    50   Input ~ 0
G2
Text GLabel 3800 3000 0    50   Input ~ 0
G1
Text GLabel 3800 3100 0    50   Input ~ 0
G0
Text GLabel 3800 3200 0    50   Input ~ 0
R3
Text GLabel 2300 3100 2    50   Input ~ 0
R2
Text GLabel 3800 3300 0    50   Input ~ 0
R2
Text GLabel 2300 3200 2    50   Input ~ 0
R1
Text GLabel 3800 3400 0    50   Input ~ 0
R1
Text GLabel 3800 3500 0    50   Input ~ 0
R0
Text GLabel 3800 3600 0    50   Input ~ 0
HSYNC
Text GLabel 3800 3700 0    50   Input ~ 0
VSYNC
Text GLabel 2300 6850 2    50   Input ~ 0
DPAD_L
Text GLabel 2300 6650 2    50   Input ~ 0
DPAD_U
Text GLabel 2300 6450 2    50   Input ~ 0
DPAD_D
Text GLabel 2300 6250 2    50   Input ~ 0
DPAD_R
Text GLabel 2300 6050 2    50   Input ~ 0
BTN_PWR
Text GLabel 2300 5850 2    50   Input ~ 0
BTN_OPT
Text GLabel 2300 5450 2    50   Input ~ 0
BTN_B
Text GLabel 2300 5650 2    50   Input ~ 0
BTN_A
Text GLabel 3800 6000 0    50   Input ~ 0
DPAD_L
Text GLabel 3800 5900 0    50   Input ~ 0
DPAD_U
Text GLabel 3800 5800 0    50   Input ~ 0
DPAD_D
Text GLabel 3800 5700 0    50   Input ~ 0
DPAD_R
Text GLabel 3800 5600 0    50   Input ~ 0
BTN_PWR
Text GLabel 3800 5500 0    50   Input ~ 0
BTN_OPT
Text GLabel 3800 5300 0    50   Input ~ 0
BTN_B
Text GLabel 3800 5400 0    50   Input ~ 0
BTN_A
Wire Wire Line
	6600 2850 6700 2850
Text GLabel 6600 2850 0    50   Input ~ 0
GND
$Comp
L power:GND #PWR?
U 1 1 5F627918
P 6700 2850
F 0 "#PWR?" H 6700 2600 50  0001 C CNN
F 1 "GND" H 6705 2677 50  0000 C CNN
F 2 "" H 6700 2850 50  0001 C CNN
F 3 "" H 6700 2850 50  0001 C CNN
	1    6700 2850
	1    0    0    -1  
$EndComp
$EndSCHEMATC
