EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x20_Female J6
U 1 1 5F4F4FBC
P 6600 3650
F 0 "J6" H 6492 2425 50  0000 C CNN
F 1 "Immobilizer Interface" H 6492 2516 50  0000 C CNN
F 2 "Connector_FFC-FPC:Hirose_FH12-20S-0.5SH_1x20-1MP_P0.50mm_Horizontal" H 6600 3650 50  0001 C CNN
F 3 "~" H 6600 3650 50  0001 C CNN
	1    6600 3650
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Female J1
U 1 1 5F4F5217
P 4900 2850
F 0 "J1" H 4792 3135 50  0001 C CNN
F 1 "D-PAD" H 4792 3044 50  0000 C CNN
F 2 "ngpc-breakout:Dpad" H 4900 2850 50  0001 C CNN
F 3 "~" H 4900 2850 50  0001 C CNN
	1    4900 2850
	-1   0    0    -1  
$EndComp
Wire Wire Line
	5100 2750 6400 2750
Wire Wire Line
	5100 2850 6250 2850
Wire Wire Line
	5100 3050 6050 3050
Wire Wire Line
	6050 3050 6050 3350
Wire Wire Line
	6050 3350 6400 3350
$Comp
L Connector:Conn_01x01_Female J2
U 1 1 5F4F8F5D
P 4900 4050
F 0 "J2" H 4792 3825 50  0001 C CNN
F 1 "BTN_A" H 4792 3916 50  0000 C CNN
F 2 "ngpc-breakout:TestPoint_Pad_2.0x4.0mm" H 4900 4050 50  0001 C CNN
F 3 "~" H 4900 4050 50  0001 C CNN
	1    4900 4050
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x01_Female J3
U 1 1 5F4F97B8
P 4900 4350
F 0 "J3" H 4792 4125 50  0001 C CNN
F 1 "BTN_B" H 4792 4216 50  0000 C CNN
F 2 "ngpc-breakout:TestPoint_Pad_2.0x4.0mm" H 4900 4350 50  0001 C CNN
F 3 "~" H 4900 4350 50  0001 C CNN
	1    4900 4350
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x01_Female J4
U 1 1 5F4F9C8C
P 4900 3750
F 0 "J4" H 4792 3525 50  0001 C CNN
F 1 "BTN_OPT" H 4792 3616 50  0000 C CNN
F 2 "ngpc-breakout:TestPoint_Pad_2.0x4.0mm" H 4900 3750 50  0001 C CNN
F 3 "~" H 4900 3750 50  0001 C CNN
	1    4900 3750
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x01_Female J5
U 1 1 5F4F9FA8
P 4900 3450
F 0 "J5" H 4792 3225 50  0001 C CNN
F 1 "BTN_PWR" H 4792 3316 50  0000 C CNN
F 2 "ngpc-breakout:TestPoint_Pad_2.0x4.0mm" H 4900 3450 50  0001 C CNN
F 3 "~" H 4900 3450 50  0001 C CNN
	1    4900 3450
	-1   0    0    1   
$EndComp
Wire Wire Line
	5800 3450 5800 3550
Wire Wire Line
	5800 3550 6400 3550
Wire Wire Line
	5800 4050 5800 3950
Wire Wire Line
	5800 3950 6400 3950
Wire Wire Line
	5800 4350 5800 4150
Wire Wire Line
	5800 4150 6400 4150
Wire Wire Line
	5100 3450 5800 3450
Wire Wire Line
	5100 4050 5800 4050
Wire Wire Line
	5100 4350 5800 4350
Wire Wire Line
	5100 3750 6400 3750
Wire Wire Line
	6250 3150 6400 3150
Wire Wire Line
	6250 2850 6250 3150
Wire Wire Line
	5100 2950 6400 2950
$Comp
L Device:Battery_Cell BT1
U 1 1 5F51375B
P 6500 2200
F 0 "BT1" H 6618 2296 50  0000 L CNN
F 1 "Battery_Cell" H 6618 2205 50  0000 L CNN
F 2 "Battery:BatteryHolder_Keystone_1058_1x2032" V 6500 2260 50  0001 C CNN
F 3 "~" V 6500 2260 50  0001 C CNN
	1    6500 2200
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Female J7
U 1 1 5F514356
P 5850 2000
F 0 "J7" H 5742 1775 50  0001 C CNN
F 1 "LiBT+" H 5742 1866 50  0000 C CNN
F 2 "ngpc-breakout:TestPoint_Pad_2.0x4.0mm" H 5850 2000 50  0001 C CNN
F 3 "~" H 5850 2000 50  0001 C CNN
	1    5850 2000
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x01_Female J8
U 1 1 5F514DE3
P 5850 2300
F 0 "J8" H 5742 2075 50  0001 C CNN
F 1 "LiBT-" H 5742 2166 50  0000 C CNN
F 2 "ngpc-breakout:TestPoint_Pad_2.0x4.0mm" H 5850 2300 50  0001 C CNN
F 3 "~" H 5850 2300 50  0001 C CNN
	1    5850 2300
	-1   0    0    1   
$EndComp
Wire Wire Line
	6050 2000 6500 2000
Wire Wire Line
	6050 2300 6500 2300
$EndSCHEMATC
