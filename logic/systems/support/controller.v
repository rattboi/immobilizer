module controller(
   input clk,
   input rst,
	input wire [5:0] pad_in,
	output reg pad_sel,

   // up, down, left, right, a, b, c, start
   output reg [7:0] controls
	);

   reg [16:0] cnt;

   always @(posedge clk) begin
      if (rst == 1'b1) begin
         pad_sel <= 1'b0;
         cnt <= 0;
      end

      cnt <= cnt + 1;

      // it's been a while, read and swap sel
      if (cnt == 0) begin
         case (pad_sel)
            1'b0: 
            begin
               controls[7] <= ~pad_in[0];
               controls[6] <= ~pad_in[1];
               controls[3] <= ~pad_in[4];
               controls[0] <= ~pad_in[5];
            end
            1'b1: 
            begin
               controls[7] <= ~pad_in[0];
               controls[6] <= ~pad_in[1];
               controls[5] <= ~pad_in[2];
               controls[4] <= ~pad_in[3];
               controls[2] <= ~pad_in[4];
               controls[1] <= ~pad_in[5];
            end
         endcase

         pad_sel <= ~pad_sel;
      end
   end

endmodule
