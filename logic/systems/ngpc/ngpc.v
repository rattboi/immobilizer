module NGPCTV
(
   input CLOCK_12
   , output [5:0] VGA_R
   , output [5:0] VGA_G
   , output [5:0] VGA_B
   , output reg VGA_VS
   , output reg VGA_HS
   , output [17:0] SRAM_ADDR
   , inout [15:0] SRAM_DQ
   , input [21:0] LCD_IN
   , output RAMOE
   , output RAMWE
   , output RAMCS
   , output RAMUB
   , output RAMLB
   , output PAD_SEL
   , input [5:0] PAD_IN
   , output [15:8] CNT_OUT
);

    wire [15:0] data_pins_in;
    wire [15:0] data_pins_out;
    wire data_pins_out_en;

    SB_IO #(
        .PIN_TYPE(6'b 1010_01),
    ) sram_data_pins [15:0] (
        .PACKAGE_PIN(SRAM_DQ),
        .OUTPUT_ENABLE(~RAMWE),
        .D_OUT_0(data_pins_out),
        .D_IN_0(data_pins_in),
    );

reg VGADIV;
reg [9:0] VCOUNT = 0;
reg [9:0] HCOUNT = 0;
reg VIDOUT = 0;
reg [11:0] PIXEL_IN;
reg [11:0] PIXEL_OUT;
reg [3:0] NGPC_HSYNC_SR;
reg [3:0] NGPC_VSYNC_SR;
reg [1:0] NGPC_DCLK_SR;
reg [17:0] WRITE_ADDR;
reg [17:0] NEXT_WRITE_ADDR;
reg [17:0] READ_ADDR;
reg [17:0] NEXT_READ_ADDR;
reg WRITE_FLAG;
reg [2:0] CYCLE;  // 0~5
reg [1:0] VSCALE;

assign RAMCS = 1'b0;
assign RAMUB = 1'b0;
assign RAMLB = 1'b0;

wire NGPC_HSYNC;
wire NGPC_VSYNC;
wire NGPC_DCLK;

assign NGPC_HSYNC = LCD_IN[8];   // Active high
assign NGPC_VSYNC = LCD_IN[7];   // Active low
assign NGPC_DCLK = LCD_IN[21];

assign VGA_R = VIDOUT ? {PIXEL_OUT[11:9], 3'h0} : 6'h0;
assign VGA_G = VIDOUT ? {PIXEL_OUT[ 7:5], 3'h0} : 6'h0;
assign VGA_B = VIDOUT ? {PIXEL_OUT[ 3:1], 3'h0} : 6'h0;

// PLL to get 50MHz clock
wire       sysclk;
wire       locked;
pll myPLL (.clock_in(CLOCK_12), .global_clock(sysclk), .locked(locked));

assign SRAM_ADDR = (CYCLE < 3'd3) ? READ_ADDR : WRITE_ADDR;

assign RAMWE = ~(WRITE_FLAG && (CYCLE == 3'd4));
assign RAMOE = ~(CYCLE == 3'd1);
assign data_pins_out = RAMOE ? {4'b0000, PIXEL_IN} : 16'bzzzzzzzzzzzzzzzz;

controller genesis_controller(
   .clk(CLOCK_12),
   .rst(1'b0),
	.pad_in(PAD_IN),
	.pad_sel(PAD_SEL),

   // up, down, left, right, a, b, c, start
   .controls({CNT_OUT[13],CNT_OUT[14],CNT_OUT[12],CNT_OUT[15],CNT_OUT[9],CNT_OUT[8],CNT_OUT[10],CNT_OUT[11]})
   );

always @(posedge sysclk)
begin
   VGADIV <= ~VGADIV;

   // Detect NGPC VSYNC falling edge
   if (NGPC_VSYNC_SR == 4'b1100)
   begin
      NEXT_WRITE_ADDR <= 18'h00000;
   end

   // Detect NGPC HSYNC rising edge
   if (NGPC_HSYNC_SR == 4'b0011)
   begin
      WRITE_ADDR <= NEXT_WRITE_ADDR;
      NEXT_WRITE_ADDR <= NEXT_WRITE_ADDR + 18'd160;
   end

   // Detect NGPC DCLK rising edge
   if ((NGPC_DCLK_SR == 2'b01) && (!WRITE_FLAG))
   begin
      WRITE_FLAG <= 1'b1;
      PIXEL_IN <= {
         {LCD_IN[12:09]},
         {LCD_IN[16:13]},
         {LCD_IN[20:17]}
      };
   end

   // SRAM access cycle control
   if (CYCLE == 3'd1)
   begin
      PIXEL_OUT <= data_pins_in[11:0];
   end
   else if (CYCLE == 3'd5)
   begin
      if (WRITE_FLAG)
      begin
         WRITE_FLAG <= 1'b0;
         WRITE_ADDR <= WRITE_ADDR + 1'b1;
      end
   end

   if (CYCLE == 3'd5)
      CYCLE <= 3'd0;
   else
      CYCLE <= CYCLE + 1'b1;

   NGPC_DCLK_SR = {NGPC_DCLK_SR[0], NGPC_DCLK};
   NGPC_HSYNC_SR = {NGPC_HSYNC_SR[2:0], NGPC_HSYNC};
   NGPC_VSYNC_SR = {NGPC_VSYNC_SR[2:0], NGPC_VSYNC};

   // 25MHz
   if (VGADIV)
   begin
      if ((CYCLE == 3'd3) && VIDOUT)
         READ_ADDR <= READ_ADDR + 1'b1;

      // VGA sync gen
      if (HCOUNT < 800)
      begin
         if (HCOUNT < 640)
         begin
            if ((HCOUNT >= 80) && (HCOUNT < 559))
            begin
               if ((VCOUNT >= 12) && (VCOUNT < 467))
                  VIDOUT <= 1;
               else
                  VIDOUT <= 0;
            end
            else
            begin
               VIDOUT <= 0;
            end
            VGA_HS <= 1;
         end
         else if ((HCOUNT >= 640) && (HCOUNT < 664))
         begin
            VIDOUT <= 0;
            VGA_HS <= 1;
         end
         else if ((HCOUNT >= 656) && (HCOUNT < 752))
         begin
            VIDOUT <= 0;
            VGA_HS <= 0;
         end
         else if (HCOUNT >= 760)
         begin
            VIDOUT <= 0;
            VGA_HS <= 1;
         end
         HCOUNT <= HCOUNT + 1;
      end
      else
      begin
         // New raster line
         HCOUNT <= 0;

         READ_ADDR <= NEXT_READ_ADDR;
         if ((VCOUNT >= 12) && (VCOUNT < 467))
         begin
            if (VSCALE == 2'd2)
            begin
               VSCALE <= 2'd0;
               NEXT_READ_ADDR <= NEXT_READ_ADDR + 18'd160;
            end
            else
               VSCALE <= VSCALE + 1'b1;
         end

         if (VCOUNT < 525)
         begin
            VCOUNT <= VCOUNT + 1;
            if ((VCOUNT >= 490) && (VCOUNT < 493))
            begin
               VGA_VS <= 0;
            end
            else
            begin
               VGA_VS <= 1;
            end
         end
         else
         begin
            //New frame
            VCOUNT <= 0;
            VSCALE <= 0;

            NEXT_READ_ADDR <= 18'h00000;
         end
      end
   end
end

always @(posedge CLOCK_12)
begin

end

endmodule
