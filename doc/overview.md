# General Design Guidelines

Overview
========
Basic concept is:
* Mostly passive interface inside consoles (portables first)
    * Video        
        * ffc/fpc lcd passthrough
        * custom fpc breakout?
    * Controls
        * probably need to tap into each input (button pad?) individually, so custom fpc or point-to-point soldering to breakout board
    * Audio
        * probably fine to leave out, and use headphone port
        * the rest of the interface is digital, so not sure about routing analog signal alongside
        * maybe ok if it's differential pair/unamplified?
    * Route to some as-yet-undefined connector to outside the console
        * Same connector for all consoles

* Separate box outside the console, shared by all modded portables
    * Connects to as-yet-undefined connector on portable
    * Interface has video input, controls output
    * FPGA 
        * ice40 line
        * ECP5 line
    * Bus Voltage Translation
        * Must support 5v IO
    * Video out
        * VGA/240p main targets
        * Can use same hd15 (standard VGA) plug for both
    * Controller in
        * Probably db9/db15
            * easy to source
            * lots of other adapters to db9/db15
            * db9 native
                * genesis
            * db15 native
                * neo geo
    * SRAM
        * Need a place for video buffer (for some consoles, at least)
            * Example: Lynx/Wonderswan vertical mode. No way to sync clockrates when rastering is in different directions...
            * Example: DS will need a buffer if we're going to show both screens on one display in 480p    
         * Shouldn't be necessary if synchronized and line-doubling. Would only need to buffer 1-2 lines then, which can fit into the limited BRAM.
    * Programming interface
        * Undetermined, but try to make it easy
        * JTAG-ish (Olimex uses this, 3.3v PGM1 interface)
        * USB
        * ESP32 (probably simplest for end-users)

Open Source 
===========
* Gateware: yosys/arachne-pnr/next-pnr only realistic options
* PCB/Schematic: KiCAD
* Mechanical: OpenSCAD? Fits DVCS better than most, as text-based/diffable.
* DVCS: git, obviously

Inspiration Projects
====================
* [Virtual Tap](https://github.com/furrtek/VirtualTap/): Furrtek's project is direct inspiration for this. I've referred to his schematic and code many times. He also gave me the initial code for lynx + npc.
* [DCHDMI](https://github.com/citrus3000psi/DCHDMI-Hardware/): This project is in KiCAD and also very fancy. It is the first KiCAD project I've seen that has a custom FPC cable, so that'll definitely come in handy when I make custom breakouts for each console. Also, I love the esp32 firmware upgrade idea and want to steal it :smile: 
* [MiSTer](https://github.com/MiSTer-devel/Hardware_MiSTer): mostly open-source, uses off-the-shelf devboard + extras, probably can borrow it's 6-bit VGA R-2R ladder.

Cost
====
* Predominant expenses in black box - $50 for hx8k breakout, fairly minimal for the other parts ($20-30 estimate?)
* Try and keep cost down for per-device modifications
    * Lean toward passive digital taps
    * If custom FPCs need made, this could increase the cost quite a bit. Haven't made one, but I assume if we can make them at JLC or PCBWay, it won't be too bad, especially if they're component-less. Gonna guess $20 max for this, per console.

Interface
=========
Bus width is something to figure out, for sure. 
The wider the bus, the bigger the connector, in general. 
Current estimate: 22 for display, 16 for controls = 38 signals

## Examples of signal use
Gonna use smallest and largest examples to feel it out. 

### Video in

Smallest: Gameboy? Around 5-6 signals
* 2-bit color interface 
* couple control lines
* lcd pixel clock 

Largest: 2ds? 30+ signals
* 24-bit color interface
* Some number of control lines per screen
* 2x pixel clocks for two different screens

Normal: Neogeo Pocket Color, 15 signals
* 12-color, individual signals
* 2 control lines (hysnc/vsync)
* 1 pixel clock

[SN74LVC16244A](http://www.ti.com/product/SN74LVC16244A) seems like a good fit 
* same as Virtual Tap
* 2x chips to cover up to 32 video-in lines (too many, but 16 is too few)
* Support both 5v and 3.3v input

### Controls out

Smallest: Gameboy, 8 signals
* 4 directions, a, b, start, select

Largest: 2DS, 21ish signals
* 4 directions
* a/b/x/y/l/r
* start/select
* home
* control stick (analog) (4-wire?)
* touchscreen (4-wire resistive analog)

Normal: Wonderswan? 12 signals
* 4 directions x2
* a/b/start/select

[sn74ahct245](http://www.ti.com/lit/ds/symlink/sn74ahct245.pdf) is probably a good chip to use
* same as Virtual Tap
* 2x chips for 16x outputs (not enough for 2DS, but it was already going to be an outlier)
* Can use Vcc from the console as the single supply
    * Automatically using the right voltage level for the system (Voh for chip is Vcc)

Video Out
=========

HD15 Connector, probably. Or HD15 and genesis-2 mdin9
Use probably 6-bit per channel Vga, as that's good enough for Mister.
That's still 20 signals though.

Since targeting 480p max, we may be able to buffer the output and run at a faster clockrate. 480p is 25Mhz pixel clock, so a 2-4x clock rate increase to handle the multiplexing seems very possible and lowers signal count by a lot.

[R-2R ladder](https://www.tek.com/blog/tutorial-digital-analog-conversion-r-2r-dac), same as MiSTer IO board should be fine. [Mister IO schematics](https://github.com/MiSTer-devel/Hardware_MiSTer/blob/master/releases/iobrd_6.1.pdf) say they used both 240/475ohm pairs, as well as 300/604ohm.

It seems prudent to also use a [THS7374 video buffer](http://www.ti.com/lit/ds/symlink/ths7374.pdf) for RGBs.

SRAM
====

[IS61WV6416BLL ](https://www.mouser.com/ProductDetail/ISSI/IS61WV6416BLL-12TLI?qs=YdQ7Kj7W0bzJTE8xxzDJIA%3D%3D) is what VirtualTap uses. Should work for us too.
If we really wanna support 18-bit color (or up to 24bit color for the 2ds?), we would need to use a wider bus chip, or we could just use 2x of these chips.

FPGA
====

The [olimex devboard](https://www.olimex.com/wiki/ICE40HX1K-EVB#Get_started_under_Linux) is using the ICE40HX1K-VQ100. I initially wanted to use the HX1K line because that's the only one that came in a lower pitch package that had any chance of being hand-soldered. The VQFP-100 is already pretty small pitch, so perhaps it's worth looking at the next step up, the 144-pin package. That would allow using the 4k or 8k models, which gives more room for fancy designs.

Olimex board also has easy-to-use expansion boards, although they don't work together (conflicting pin assignments)
* [iCE40-DIO](https://www.olimex.com/Products/FPGA/iCE40/iCE40-DIO/open-source-hardware): Digital IO board w/ adjustable DAC for Vref
* [iCE40-IO](https://www.olimex.com/Products/FPGA/iCE40/iCE40-IO/open-source-hardware): 3-bit VGA, PS/2 input, irDA

The [HX8K Breakout Board](http://www.latticesemi.com/en/Products/DevelopmentBoardsAndKits/iCE40HX8KBreakoutBoard.aspx) made by Lattice has a lot of advantages similar to MiSTer's DE10-nano: you can buy it off-the-shelf, from many distributors, and it's the most complex portion of the electronics. I am inclined now to make a daughterboard for the breakout that has all the peripheral electronics on it. The HX8K Breakout Board is using the iCE40HX-8k CT256, which natively has ~200 IO pins available, although the breakout only makes ~115 of those available. I believe 115 is enough for most things, and a great starting point.

[How to blink an LED w/ hx8k breakout (also has toolchain setup)](https://mjoldfield.com/atelier/2018/02/ice40-blinky-hx8k-breakout.html) 

Quick breakdown:
- VGA out: (6bitx3) + 2 control signals (hs/vs) = 20 IO used
- SRAM: 16 address lines + 16 data lines + 2 control lines = 34 IO used 
- Controller In: let's give 16 lines so that db15 fully parallel is an option = 16 IO used
- Controls Out: let's give 16 lines here as well, just because = 16 IO used

That leaves the rest for video in from the console
115 - VGA - SRAM - ControlIn - ControlOut = 29 IO left

For DS, I believe that will use 20, so that still leaves some spare (although not a lot)

