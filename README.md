# Immobilizer Readme
------------------------

0. Disclaimer

You use it at your own risk, there's no warranty.

1. License

- All HDL (unless otherwise noted) is released under LGPL v3.0
- All software (unless otherwise noted), is GPL v 3.0 or later.
- PCB design and schematics are licensed under CERN Open Hardware License v 1.2.

2. Acknowledgements

- Special thanks go to Furrtek, for his donation of the original lynx and ngpc code, and
the many times I've referenced his [VirtualTap](https://github.com/furrtek/virtualtap) project

